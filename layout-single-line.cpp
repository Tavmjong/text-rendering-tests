
#include "layout-single-line.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

#include <pangomm.h>
#include <pango/pangocairo.h>

#include "font-instance.h"
#include "font-factory.h"
#include "layout-style.h"

#include <cassert>

// COPIED FROM pangofc-shape.c
static void
apply_extra_attributes (GSList       *attrs,
                        hb_feature_t *features,
                        guint         length,
                        guint        *num_features)
{
  GSList *l;

  for (l = attrs; l && *num_features < length; l = l->next)
    {
      PangoAttribute *attr = (PangoAttribute *)l->data;
      if (attr->klass->type == PANGO_ATTR_FONT_FEATURES)
        {
          PangoAttrFontFeatures *fattr = (PangoAttrFontFeatures *) attr;
          const gchar *feat;
          const gchar *end;
          int len;

          feat = fattr->features;

          while (feat != NULL && *num_features < length)
            {
              end = strchr (feat, ',');
              if (end)
                len = end - feat;
              else
                len = -1;
              if (hb_feature_from_string (feat, len, &features[*num_features]))
                {
                  features[*num_features].start = attr->start_index;
                  features[*num_features].end = attr->end_index;
                  (*num_features)++;
                }

              if (end == NULL)
                break;

              feat = end + 1;
            }
        }
    }

  /* Turn off ligatures when letterspacing */
  for (l = attrs; l && *num_features < length; l = l->next)
    {
      PangoAttribute *attr = (PangoAttribute *)l->data;
      if (attr->klass->type == PANGO_ATTR_LETTER_SPACING)
        {
          hb_tag_t tags[] = {
            HB_TAG('l','i','g','a'),
            HB_TAG('c','l','i','g'),
            HB_TAG('d','l','i','g'),
          };
          int i;
          for (i = 0; i < G_N_ELEMENTS (tags); i++)
            {
              features[*num_features].tag = tags[i];
              features[*num_features].value = 0;
              features[*num_features].start = attr->start_index;
              features[*num_features].end = attr->end_index;
              (*num_features)++;
            }
        }
    }
}

// Utility function
std::string gravity_to_string(int gravity)
{
    std::string out;
    switch (gravity) {
        case PANGO_GRAVITY_SOUTH: out = "South"; break;
        case PANGO_GRAVITY_EAST:  out = "East";  break;
        case PANGO_GRAVITY_NORTH: out = "North"; break;
        case PANGO_GRAVITY_WEST:  out = "West";  break;
        case PANGO_GRAVITY_AUTO:  out = "Auto";  break;
        default: out = "Unhandled gravity";
    }
    return out;
}

std::string gravity_hint_to_string(int hint)
{
    std::string out;
    switch (hint) {
        case PANGO_GRAVITY_HINT_NATURAL: out = "Natural"; break;
        case PANGO_GRAVITY_HINT_STRONG:  out = "Strong";  break;
        case PANGO_GRAVITY_HINT_LINE:    out = "Line";    break;
    }
    return out;
}

Layout::Layout (PangoFontMap* server)
    : _server (server)
{
    assert (server != nullptr);
    PangoContext* gcontext(pango_font_map_create_context(server));
    _pango_context = Glib::wrap(gcontext);
}

void
Layout::set_base_direction (Pango::Direction base_direction)
{
    _base_direction = base_direction;
    _pango_context->set_base_dir (base_direction);
}

void
Layout::set_base_gravity (Pango::Gravity base_gravity)
{
    std::cout << "Layout::set_base_gravity: " << gravity_to_string (base_gravity) << std::endl;
    _base_gravity = base_gravity;
    _pango_context->set_base_gravity (base_gravity);

    if (base_gravity != Pango::GRAVITY_AUTO) {  // Will abort if auto, see Pango documentation.
        auto attr_base_gravity = Pango::AttrInt::create_attr_gravity(base_gravity);
        _attrs.change(attr_base_gravity);
    }
}

void
Layout::set_gravity_hint (Pango::GravityHint gravity_hint)
{
    _gravity_hint = gravity_hint;
    _pango_context->set_gravity_hint (gravity_hint);

    auto attr_gravity_hint  = Pango::AttrInt::create_attr_gravity_hint(gravity_hint);
    _attrs.change(attr_gravity_hint);
}

// Add text to be rendered along with style and SVG positioning attributes.
// Note: SVG positioning attributes must already be cascaded properly.
void
Layout::add_text(const Glib::ustring& text, SPStyle* style,
                 std::vector<SVGLength> x,
                 std::vector<SVGLength> y,
                 std::vector<SVGLength> dx,
                 std::vector<SVGLength> dy,
                 std::vector<SVGLength> rotate)
{
    std::cout << "Layout::add_text: Entrance: " << text << std::endl;

    // Each character with an SVG positioning attribute must create a new span.

    // Find maximum number of characters with SVG attributes defined.
    unsigned max = std::max({x.size(), y.size(), dx.size(), dy.size(), rotate.size()});
    if (max > text.length()) {
        std::cerr << "Layout::add_text: too many position attributes!" << std::endl;
        max = text.length();
    }
    std::cout << " add_text: maximum SVG position attribute size: " << max << std::endl;

    // Add a span for each character with an attribute.
    for (unsigned i = 0; i < max; ++i) {
        auto opt_x  = boost::optional<SVGLength>();
        auto opt_y  = boost::optional<SVGLength>();
        auto opt_dx = boost::optional<SVGLength>();
        auto opt_dy = boost::optional<SVGLength>();
        auto opt_r  = boost::optional<SVGLength>();
        if (i < x.size())      { opt_x  = x[i]; }
        if (i < y.size())      { opt_y  = y[i]; }
        if (i < dx.size())     { opt_dx = dx[i]; }
        if (i < dy.size())     { opt_dy = dy[i]; }
        if (i < rotate.size()) { opt_r  = rotate[i]; }
        _spans.emplace_back(new Span(*this, text.substr(i, 1), style, opt_x, opt_y, opt_dx, opt_dy, opt_r));
    }

    // Add a span for all remaining characters.
    if (max < text.length()) {
        _spans.emplace_back(new Span(*this, text.substr(max), style));
    }
    std::cout << "Layout::add_text: Exit" << std::endl;
}


// Temp function used in revere_glyph_string()
void
dump_glyphs(PangoGlyphString* glyph_string)
{
    int num_glyphs = glyph_string->num_glyphs;
    std::cout << "  id: ";
    for (int i = 0; i < num_glyphs; ++i) {
        std::cout << " " << glyph_string->glyphs[i].glyph;
    }
    std::cout << std::endl;

    std::cout << "  start?: ";
    for (int i = 0; i < num_glyphs; ++i) {
        std::cout << " " << glyph_string->glyphs[i].attr.is_cluster_start;
    }
    std::cout << std::endl;

    std::cout << "  cluster: ";
    for (int i = 0; i < num_glyphs; ++i) {
        std::cout << " " << glyph_string->log_clusters[i];
    }
    std::cout << std::endl;
}

// We're given clusters in visual order, always left to right. We want
// them in logical order (per SVG's rendering order).
//
// Reversing clusters is complicated... so we'll reverse glyphs then
// fix the "is_cluster_start" flag.  This means that glyphs are drawn
// in reverse order within a cluster, which shouldn't matter in
// practice. If somebody does complain, this is the place to fix
// it. (Need example where there is more than one glyph in cluster.)
void
reverse_glyph_string(PangoGlyphString* glyph_string)
{
    std::cout << "Reverse glyphs" << std::endl;
    dump_glyphs(glyph_string);

    int num_glyphs = glyph_string->num_glyphs;

    for (int i = 0; i < num_glyphs/2; ++i) {
        PangoGlyphInfo temp = glyph_string->glyphs[i];
        glyph_string->glyphs[i] = glyph_string->glyphs[num_glyphs - i - 1];
        glyph_string->glyphs[num_glyphs - i - 1] = temp;

        gint t = glyph_string->log_clusters[i];
        glyph_string->log_clusters[i] = glyph_string->log_clusters[num_glyphs - i -1];
        glyph_string->log_clusters[num_glyphs - i - 1] = t;
    }

    // Fix "is_cluster_start" flag.
    for (int i = 0; i < num_glyphs; ++i) {

        glyph_string->glyphs[i].attr.is_cluster_start = 1; // Set start of cluster.

        int log_cluster_current = glyph_string->log_clusters[i];
        int j = i + 1;
        while (j < num_glyphs && glyph_string->log_clusters[j] == log_cluster_current) {
            glyph_string->glyphs[j].attr.is_cluster_start = 0; // Not start of cluster.
            ++j;
        }
        i = j; // Move to next cluster.
    }

    dump_glyphs(glyph_string);
}

void dump_attributes(Pango::AttrList& attributes, Glib::ustring tag = Glib::ustring());

// Itemize and Shape text.
// We can only itemize after all spans are created else _text does not
// contain the characters that follow the span which are needed for
// proper shaping.
//
// When we itemize and shape, we do so with the proper font size. But,
// we create the FontInstance with a generic size font.
void
Layout::itemize()
{
    // Paragraph layout.
    std::cout << "  Base direction: ";
    switch (_base_direction) {
        case Pango::DIRECTION_LTR:  // Documentation has Pango::Direction::LTR
            std::cout << "LTR" << std::endl;
            break;
        case Pango::DIRECTION_RTL:
            std::cout << "RTL" << std::endl;
            break;
        default:
            std::cout << "Unhandled value!" << std::endl;
    }
    
    // Gravity
    std::cout << "  Base gravity: " << gravity_to_string (_base_gravity) << std::endl;

    // Get character logical attributes for entire text (C++ API is tied to Pango::Layout which we don't use).
    _char_attributes.resize(_text.size() + 1);
    pango_get_log_attrs(_text.data(), _text.bytes(), -1, nullptr, &*_char_attributes.begin(), _char_attributes.size());

    // Look at attributes:
    dump_attributes(_attrs);

    // Itemize span by span (things like discretionary ligatures should not cross over spans).
    Pango::AttrIter cached_iter = _attrs.get_iter(); // Points to start of list!
    for (auto& span : _spans) {
        std::cout << "\n******************\nItemizing span" << std::endl;

        int index_char = span->_start_index_char; // Used to index into character logical attributes vector.

        // Itemize.
        span->_pango_items =
            _pango_context->itemize(_text, span->_start_index_byte, span->_length_byte, _attrs, cached_iter);

        // Shape and store results.
        for (auto& pango_item : span->_pango_items) {
            dump_item(pango_item);
            _text_items.emplace_back  (new Layout::TextItem (&pango_item, span, index_char, this));
            index_char += pango_item.get_num_chars();
        }
    } // Span
}

// Utility function for debugging rendering.
void
draw_square(Cairo::RefPtr<Cairo::Context> context, double font_size, bool rtl)
{
    context->save();

    int width = 2000 * (rtl?-1:1);
    // Draw some guide lines
    context->move_to(0,     0);
    context->line_to(width, 0);
    context->move_to(0,     -font_size);
    context->line_to(width, -font_size);
    context->set_source_rgb(0.5, 0.5, 1.0);
    context->stroke();

    // Draw some boxes.
    context->set_source_rgb(0.7, 0.7, 1.0);
    for (int i = 0; i < 20; ++i) {
        context->rectangle(0, -font_size/2.0, (rtl ? -font_size : font_size), font_size);
        context->stroke();
        int dx = font_size * (rtl?-1:1);
        context->translate(dx, 0);
    }

    context->restore();

    // Draw square at origin.
    context->rectangle(-3, -3, 6, 6);
    context->set_source_rgb(1.0, 0.0, 0.0);
    context->fill();

    // Prepare for drawing text.
    context->set_source_rgb(0.0, 0.0, 0.0);
}


// Render text to test Cairo png.
void
Layout::render(Cairo::RefPtr<Cairo::Context>& context)
{
    std::cout << "\nRendering!!" << std::endl;

    bool is_vertical = false;
    if (_base_gravity == Pango::GRAVITY_EAST || _base_gravity == Pango::GRAVITY_WEST) {
        is_vertical = true; // This is only for layout purposes... doesn't handle "sideways"
    }
    std::cout << " Base gravity: " << gravity_to_string(_base_gravity)
              << "  Gravity hint: " << (int)_gravity_hint
              << std::endl;

    std::cout << " Base direction: ";
    switch (_base_direction) {
        case Pango::DIRECTION_LTR:  // Documentation has Pango::Direction::LTR
            std::cout << "LTR" << std::endl;
            break;
        case Pango::DIRECTION_RTL:
            std::cout << "RTL" << std::endl;
            break;
        default:
            std::cout << "Unhandled!" << std::endl;
    }

    int x = 0;
    int y = 0;
    int line_spacing = 150;

    {

        // ======= Pango GlyphString to Cairo glyph (HarfBuzz font)  =========
        std::cout << "\n111  Pango GlyphString to Cairo glyph (HarfBuzz font)" << std::endl;

        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            if (is_vertical) {
                // We draw vertical text sideways to match how Inkscape (and Pango) handle vertical text.
                context->rotate(-M_PI/2.0);
            }

            double xx = 0.0;  // Horizontal direction (before any rotation).
            double yy = 0.0;

            bool is_opposite = false;
            bool is_first_cluster = true; // Used to calculate buffer advance (correct for letter spacing).

            double buffer_advance = 0.0;
            int i_item = 0;
            int max_item = _text_items.size();
            for (auto text_item : _text_items) {
                auto glyphstring = text_item->_pango_glyph_string;

                auto style = text_item->_span->get_style();
                double font_size      = style->get_font_size();
                double letter_spacing = style->get_letter_spacing();
                double word_spacing   = style->get_word_spacing();

                // No font substitution is done! We need to use correct font, which we can get from the
                // Pango::Item used to create the Pango::GlyphString.
                auto pango_item = text_item->_pango_item;
                int gravity = pango_item->get_analysis().gobj()->gravity;
                std::cout << "111  Gravity: " << gravity_to_string(gravity) << std::endl;

                auto pango_font = pango_item->get_analysis().get_font();
                auto hb_font = pango_font_get_hb_font(pango_font->gobj());

                // For right-to-left text we need to reverse the order of drawing the glyphs.
                // We need to move to the end of the "buffer advance" and work backwards.
                // We calculate the buffer advance here.
                int level = pango_item->get_analysis().get_level();
                bool is_rtl = (level & 1); // Odd is rtl.
                if ( is_rtl && _base_direction == Pango::DIRECTION_LTR ||
                     !is_rtl && _base_direction == Pango::DIRECTION_RTL) {

                    std::cout << "111 Is opposite!" << std::endl;
                    if (!is_opposite) {
                        is_opposite = true;
                        std::cout << "111  Is opposite flag not set, setting." << std::endl;

                        // Find buffer advance:
                        int j_item = i_item;
                        while (j_item < max_item && level == _text_items[j_item]->_pango_item->get_analysis().get_level()) {
                            // Find advance for one item/glyph string.
                            for (auto glyph_info : _text_items[j_item]->_pango_glyph_string.get_glyphs()) {
                                Pango::GlyphGeometry glyph_geom = glyph_info.get_geometry(); // Width, x_offset, y_offset.
                                double width = glyph_geom.get_width()    / (double)PANGO_SCALE;
                                buffer_advance += width;
                            }

                            std::cout << "111 adding " << _text_items[j_item]->get_letter_count() << " letter spaces!" << std::endl;
                            buffer_advance += letter_spacing * _text_items[j_item]->get_letter_count();
                            buffer_advance += word_spacing   * _text_items[j_item]->get_word_count();

                            if (is_first_cluster) {
                                is_first_cluster = false;
                                std::cout << "111 subtracting letter/word spacing" << std::endl;
                                buffer_advance -= letter_spacing;
                                buffer_advance -= word_spacing;
                            }

                            ++j_item;
                        }

                        if (is_vertical) {
                            yy += buffer_advance * (is_rtl?1:-1);
                        } else {
                            xx += buffer_advance * (is_rtl?1:-1);
                        }
                    } // is opposite

                } else {
                    std::cout << "111 Is not opposite" << std::endl;

                    if (is_opposite) {
                        std::cout << "111  Is opposite flag set, unsetting." << std::endl;
                        is_opposite = false;

                        if (is_vertical) {
                            yy += buffer_advance * (!is_rtl?1:-1);
                        } else {
                            xx += buffer_advance * (!is_rtl?1:-1);
                        }
                        buffer_advance = 0.0;
                    }
                }

                bool has_vertical_metrics = false;
                if (is_vertical && gravity == Pango::GRAVITY_EAST) {
                    hb_font_extents_t hb_font_extents;
                    if (hb_font_get_v_extents (hb_font, &hb_font_extents)) {
                        has_vertical_metrics = true;
                    }
                }

                int hb_x_scale = 0;
                int hb_y_scale = 0;
                hb_font_get_scale (hb_font, &hb_x_scale, &hb_y_scale);

                double cluster_offset = 0.0;
                std::vector<Cairo::Glyph> cairo_glyphs;
                for (auto glyph_info : glyphstring.get_glyphs()) {
                    Pango::Glyph         glyph      = glyph_info.get_glyph();  // typedef PangoGlyph  guint32
                    Pango::GlyphGeometry glyph_geom = glyph_info.get_geometry(); // Width, x_offset, y_offset.
                    Pango::GlyphVisAttr  glyph_attr = glyph_info.get_attr();   // typedef PangoGlyphVisAttr (is_cluster_start)

                    double width = glyph_geom.get_width()    / (double)PANGO_SCALE;
                    double dx    = glyph_geom.get_x_offset() / (double)PANGO_SCALE;
                    double dy    = glyph_geom.get_y_offset() / (double)PANGO_SCALE;

                    if (pango_version_check(1,48,4) != nullptr && pango_version_check(1,48,1) == nullptr) {
                        // Pango is 1.48.1 through 1.48.3.
                        if (is_vertical && gravity == Pango::GRAVITY_EAST && !has_vertical_metrics) {
                            hb_position_t x_origin = 0.0;
                            hb_position_t y_origin = 0.0;
                            hb_font_get_glyph_v_origin (hb_font, glyph, &x_origin, &y_origin);
                            std::cout << "111  vertical"
                                      << " x_origin: " << std::setw(6) << x_origin / (double)hb_x_scale * font_size
                                      << " y_origin: " << std::setw(6) << y_origin / (double)hb_y_scale * font_size
                                      << " dx: "       << std::setw(6) << dx
                                      << " dy: "       << std::setw(6) << dy << std::endl;
                            dx += y_origin / (double)hb_y_scale * font_size;
                            dy += x_origin / (double)hb_x_scale * font_size;
                        }
                    }

                    PangoRectangle ink_rect;
                    PangoRectangle logical_rect;
                    pango_font_get_glyph_extents(pango_font->gobj(), glyph, &ink_rect, &logical_rect);
                    double rise = (logical_rect.y + logical_rect.height/2.0) / (double)PANGO_SCALE;

                    char glyph_name[32];
                    hb_font_get_glyph_name (hb_font, glyph, glyph_name, sizeof (glyph_name));

                    std::cout << "111  Glyph: " << std::setw(5) << glyph
                              << "  dx: "       << std::setw(8) << dx
                              << "  dy: "       << std::setw(8) << dy
                              << "  width: "    << std::setw(8) << width
                              << "  rise: "     << std::setw(8) << rise
                              << "  cluster start: " << glyph_attr.is_cluster_start
                              << "  "           << glyph_name
                              << std::endl;

                    if (is_rtl) {
                        if (is_vertical) {
                            yy -= width;
                        } else {
                            xx -= width;
                        }
                    }
          
                    // Fill cairo_glyph
                    cairo_glyph_t cairo_glyph;
                    cairo_glyph.index = glyph;
                    if (is_vertical) {
                        switch (gravity) {
                            case PANGO_GRAVITY_EAST:
                                // Glyphs have been rotated around origin (left most point on alphabetic baseline).
                                cairo_glyph.x = xx - dy;
                                if (pango_version_check(1,48,1) == nullptr) { // Pango is 1.48.1 or later (after minus sign fix).
                                    cairo_glyph.y = yy + dx;
                                } else {
                                    cairo_glyph.y = yy + (width - dx);
                                }
                                break;
                            case PANGO_GRAVITY_SOUTH:
                                // Sideways text in "mixed" glyph orientation.
                                cairo_glyph.x = xx - dy + rise;
                                cairo_glyph.y = yy + dx;  // Inline direction.
                                break;
                            default:
                                std::cout << "UNHANDLED GRAVITY!" << std::endl;
                        }
                    } else {
                        cairo_glyph.x = xx + dx;
                        cairo_glyph.y = yy + dy;
                    }

                    cairo_glyphs.push_back(cairo_glyph);

                    // Move onto next glyph
                    if (!is_rtl) {
                        if (is_vertical) {
                            yy += width;
                        } else {
                            xx += width;
                        }

                        // Apply letter & word spacing.
                        if (true) {
                            if (is_vertical) {
                                yy += letter_spacing;
                            } else {
                                xx += letter_spacing;
                            }
                        }
                    } else {
                        // Apply letter & word spacing.
                        if (true) {
                            if (is_vertical) {
                                yy -= letter_spacing;
                            } else {
                                xx -= letter_spacing;
                            }
                        }
                    }

                } // Loop over glyphs.

                // Set font
                FontInstance* font_instance =
                    FontFactory::get_instance().get_font_instance(pango_font->describe());
                font_instance->set_cairo_font (context, is_vertical, font_size);

                // Render
                Paint paint = text_item->get_style()->get_fill();
                context->set_source_rgb (paint.get_r(), paint.get_g(), paint.get_b());
                context->show_glyphs(cairo_glyphs);

                ++i_item;
            }
        }
        context->restore();

        context->translate(0, line_spacing);


        // ======= Harfbuzz buffer to Cairo glyph (HarfBuzz font) =========
        // See harfbuzz/util/helper-cairo.cc helper_cairo_line_from_buffer(). (Also for clusters.)
        std::cout << "\n222  HarfBuzz hb_buffer to Cairo glyph (HarfBuzz font)" << std::endl;

        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            if (is_vertical) {
                // We draw vertical text sideways to match how Inkscape (and Pango) handle vertical text.
                context->rotate(-M_PI/2.0);
            }
            context->set_source_rgb(0.0, 0.0, 0.0);

            double xx = 0.0; // Horizontal direction before rotation
            double yy = 0.0;

            bool is_opposite = false;
            bool is_first_cluster = true; // Used to calculate buffer advance (correct for letter spacing).

            double buffer_advance_x = 0.0;
            double buffer_advance_y = 0.0;
            int i_item = 0;
            int max_item = _text_items.size();
            for (auto text_item : _text_items) {

                auto style = text_item->_span->get_style();
                double font_size      = style->get_font_size();
                double letter_spacing = style->get_letter_spacing() * PANGO_SCALE;
                double word_spacing   = style->get_word_spacing() * PANGO_SCALE;

                auto hb_buffer = text_item->_hb_buffer;
                auto pango_item = text_item->_pango_item;
                int gravity = pango_item->get_analysis().gobj()->gravity;
                std::cout << "222 Gravity: " << gravity_to_string(gravity) << std::endl;

                // Set font
                auto pango_font = pango_item->get_analysis().get_font();

                FontInstance* font_instance =
                    FontFactory::get_instance().get_font_instance(pango_font->describe());
                font_instance->set_cairo_font (context, is_vertical, font_size);
                auto hb_font = font_instance->get_hb_font();

                bool has_vertical_metrics = false;
                if (is_vertical) {
                    hb_font_extents_t hb_font_extents;
                    if (hb_font_get_v_extents (hb_font, &hb_font_extents)) {
                        std::cout << "Found vertical font extents" << std::endl;
                        std::cout << "hb_font_extents: "
                                  << " ascender: "  << hb_font_extents.ascender  / (double)PANGO_SCALE
                                  << " descender: " << hb_font_extents.descender / (double)PANGO_SCALE
                                  << " line gap: "  << hb_font_extents.line_gap  / (double)PANGO_SCALE
                                  << std::endl;
                        has_vertical_metrics = true;
                    } else {
                        std::cout << "Did not find vertical font extents" << std::endl;
                    }
                }

                unsigned int len = hb_buffer_get_length (hb_buffer);
                hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
                hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, NULL);

                // For right-to-left text we need to reverse the order of drawing the glyphs.
                // We need to move to the end of the "buffer advance" and work backwards.
                // We calculate the buffer advance here.
                int level = pango_item->get_analysis().get_level();
                bool is_rtl = (level & 1); // Odd is rtl. (Should include gravity!)

                if ( is_rtl && _base_direction == Pango::DIRECTION_LTR ||
                     !is_rtl && _base_direction == Pango::DIRECTION_RTL) {
                    std::cout << "  Opposite direction   <--------- " << std::endl;

                    if (!is_opposite) {
                        std::cout << "    setting is_opposite true" << std::endl;
                        is_opposite = true;

                        // Find buffer advance:
                        int j_item = i_item;
                        while (j_item < max_item && level == _text_items[j_item]->_pango_item->get_analysis().get_level()) {
                            // Find advance for one item/glyph string.
                            int j_gravity = _text_items[j_item]->_pango_item->get_analysis().gobj()->gravity;
                            unsigned int len = 0;
                            // hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (span->_hb_buffers[j_item], &len);
                            hb_glyph_position_t *pos =
                                hb_buffer_get_glyph_positions (_text_items[j_item]->_hb_buffer, &len);
                            for (unsigned int g = 0; g < len; ++g) {
                                std::cout << "  Advance: " << pos[g].x_advance << " - " << pos[g].y_advance
                                          << "     Gravity: " << gravity_to_string (j_gravity) << std::endl
                                          << "     is_vertical: " << std::boolalpha << is_vertical
                                          << std::endl;
                                if (is_vertical) {
                                    switch (j_gravity) {
                                        case PANGO_GRAVITY_EAST:
                                            buffer_advance_x += pos[g].x_advance;
                                            buffer_advance_y -= pos[g].y_advance;
                                            break;
                                        case PANGO_GRAVITY_SOUTH:
                                            buffer_advance_x += pos[g].y_advance;
                                            buffer_advance_y += pos[g].x_advance;
                                            break;
                                        default:
                                            std::cout << "UNHANDLED GRAVITY!" << std::endl;
                                    }
                                } else {
                                    buffer_advance_x += pos[g].x_advance;
                                    buffer_advance_y += pos[g].y_advance;
                                }
                            }

                            if (is_vertical) {
                                buffer_advance_y += letter_spacing * _text_items[j_item]->get_letter_count();
                                buffer_advance_y += word_spacing   * _text_items[j_item]->get_word_count();
                            } else {
                                buffer_advance_x += letter_spacing * _text_items[j_item]->get_letter_count();
                                buffer_advance_x += word_spacing   * _text_items[j_item]->get_word_count();
                            }


                            if (is_first_cluster) {
                                is_first_cluster = false;
                                std::cout << "222 subtracting letter/word spacing" << std::endl;
                                if (is_vertical) {
                                    buffer_advance_y -= letter_spacing;
                                    buffer_advance_y -= word_spacing;
                                } else {
                                    buffer_advance_x -= letter_spacing;
                                    buffer_advance_x -= word_spacing;
                                }
                            }

                            ++j_item;
                        }
                        buffer_advance_x /= (double)PANGO_SCALE;
                        buffer_advance_y /= (double)PANGO_SCALE;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance_x
                                  << "  y: " << std::setw(8) << buffer_advance_y
                                  << std::endl;

                        xx += buffer_advance_x * (is_rtl ? 1 : -1);
                        yy += buffer_advance_y * (is_rtl ? 1 : -1);

                    } // is opposite

                } else {

                    if (is_opposite) {
                        std::cout << "    setting is_opposite false" << std::endl;
                        is_opposite = false;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance_x
                                  << "  y: " << std::setw(8) << buffer_advance_y
                                  << std::endl;

                        xx -= buffer_advance_x * (is_rtl ? 1 : -1);
                        yy -= buffer_advance_y * (is_rtl ? 1 : -1);

                        buffer_advance_x = 0.0;
                        buffer_advance_y = 0.0;
                    }
                }

                int hb_x_scale = 0;
                int hb_y_scale = 0;
                hb_font_get_scale (hb_font, &hb_x_scale, &hb_y_scale);
                std::cout << "222 hb_y_scale: " << hb_y_scale << std::endl;

                // Correct vertical spacing cluster-by-cluster.
                int old_cluster = -1;
                double cluster_offset = 0.0;

                // Fill cairo_glyphs
                std::vector<Cairo::Glyph> cairo_glyphs;
                for (unsigned int g = 0; g < len; ++g) {

                    int new_cluster = info[g].cluster;
                    bool start_of_cluster = false;
                    if (old_cluster != new_cluster) {
                        start_of_cluster = true;
                        old_cluster = new_cluster;
                    }

                    if (is_rtl) {
                        // Position next glyph (moving backwards)
                        if (is_vertical) {
                            switch (gravity) {
                                case PANGO_GRAVITY_EAST:
                                    xx -= pos[g].x_advance / (double)PANGO_SCALE;
                                    yy += pos[g].y_advance / (double)PANGO_SCALE;
                                    break;
                                case PANGO_GRAVITY_SOUTH:
                                    xx -= pos[g].y_advance / (double)PANGO_SCALE;
                                    yy -= pos[g].x_advance / (double)PANGO_SCALE;
                                    break;
                                default:
                                    std::cout << "UNHANDLED GRAVITY!" << std::endl;
                            }
                
                        } else {
                            xx -= pos[g].x_advance / (double)PANGO_SCALE;
                            yy -= pos[g].y_advance / (double)PANGO_SCALE;
                        }
                    }

                    // Add offsets
                    double dx = pos[g].x_offset / (double)PANGO_SCALE;
                    double dy = pos[g].y_offset / (double)PANGO_SCALE;

                    PangoRectangle ink_rect;
                    PangoRectangle logical_rect;
                    pango_font_get_glyph_extents(pango_font->gobj(), info[g].codepoint, &ink_rect, &logical_rect);
                    double rise = (logical_rect.y + logical_rect.height/2.0) / (double)PANGO_SCALE;

                    char glyph_name[32];
                    hb_font_get_glyph_name (hb_font, info[g].codepoint, glyph_name, sizeof (glyph_name));

                    std::cout << "222  Glyph: " << std::setw(5) << info[g].codepoint
                              << "  dx: "       << std::setw(8) << dx
                              << "  dy: "       << std::setw(8) << dy
                              << "  ax: "       << std::setw(8) << pos[g].x_advance / (double)PANGO_SCALE
                              << "  ay: "       << std::setw(8) << pos[g].y_advance / (double)PANGO_SCALE
                              << "  rise: "     << std::setw(8) << rise
                              << "  cluster: "  << info[g].cluster
                              << "  "           << glyph_name
                              << std::endl;


                    //if (is_vertical && !has_vertical_metrics && gravity == PANGO_GRAVITY_EAST) {
                    if (is_vertical && gravity == PANGO_GRAVITY_EAST) {
                        if (has_vertical_metrics) {
                            if (pango_version_check(1,48,4) != nullptr && pango_version_check(1,48,1) == nullptr) {
                                // Pango is 1.48.1 through 1.48.3.
                                // We've loaded the glyphs assuming vertical metrics but Harfbuzz has shaped them assuming horizontal metrics. We must "uncorrect".
                                hb_position_t x_origin = 0.0;
                                hb_position_t y_origin = 0.0;
                                hb_font_get_glyph_v_origin (hb_font, info[g].codepoint, &x_origin, &y_origin);
                                // x is half of horizontal advance
                                // y is VORG.get_y_origin() if vertical metrics, else calculated from extents.
                                // (See hb-ot-font.cc hb_ot_get_glyph_v_origin().)
                                std::cout << "222 glyph origin: " << x_origin / (double)PANGO_SCALE << ", " << y_origin / (double)PANGO_SCALE << std::endl;
                                std::cout << "222 hb_y_scale: " << hb_y_scale << std::endl;
                                dx += x_origin / (double)hb_x_scale * font_size;
                                dy += y_origin / (double)hb_y_scale * font_size;
                            }
                        } else {
                            // If a font does not have vertical metrics, the glyphs
                            // are first centered vertically with in the grid
                            // (determined by the vertical advances). Harfbuzz shaping
                            // then moves via 'y_offset' the ink rectangle to the top
                            // of the grid. This is less than ideal. We "correct" this
                            // by shifting the glyphs down so that the "em" box is
                            // aligned with the grid (assuming uniform vertical
                            // advances). This is done cluster-by-cluster.
                            if (start_of_cluster) {

                                hb_glyph_extents_t glyph_extents;
                                if (hb_font_get_glyph_extents (hb_font, info[g].codepoint, &glyph_extents)) {

                                    double baseline_adjust =
                                        font_instance->get_baseline(BASELINE_TEXT_BEFORE_EDGE) -
                                        font_instance->get_baseline(BASELINE_ALPHABETIC);
                                    std::cout << "baseline_adjust: " << baseline_adjust << std::endl;
                                    cluster_offset = ((glyph_extents.y_bearing / (double)hb_y_scale) - baseline_adjust) * font_size;
                                } else {
                                    std::cout << "Extents: failed" << std::endl;
                                    cluster_offset = 0.0;
                                }
                            }
                        }
                    }
                    dy += cluster_offset; // Use same cluster offset for all glyphs in cluster.

                    // Fill cairo_glyph
                    cairo_glyph_t cairo_glyph;
                    cairo_glyph.index = info[g].codepoint;
                    if (is_vertical) {
                        switch (gravity) {
                            case PANGO_GRAVITY_EAST:
                                cairo_glyph.x = xx + dx;
                                cairo_glyph.y = yy - dy;
                                break;
                            case PANGO_GRAVITY_SOUTH:
                                cairo_glyph.x = xx + dy;
                                cairo_glyph.y = yy + dx;
                                // Baseline shift
                                cairo_glyph.x += rise;
                                break;
                            default:
                                std::cout << "UNHANDLED GRAVITY!" << std::endl;
                        }
                    } else {
                        cairo_glyph.x = xx + dx;
                        cairo_glyph.y = yy - dy;
                    }

                    cairo_glyphs.push_back(cairo_glyph);

                    if (!is_rtl) {
                        // Move onto next glyph
                        if (is_vertical) {
                            switch (gravity) {
                                case PANGO_GRAVITY_EAST:
                                    xx += pos[g].x_advance / (double)PANGO_SCALE;
                                    yy -= pos[g].y_advance / (double)PANGO_SCALE;
                                    break;
                                case PANGO_GRAVITY_SOUTH:
                                    xx += pos[g].y_advance / (double)PANGO_SCALE;
                                    yy += pos[g].x_advance / (double)PANGO_SCALE;
                                    break;
                                default:
                                    std::cout << "UNHANDLED GRAVITY!" << std::endl;
                            }
                            if (start_of_cluster) {
                                yy += letter_spacing / (double)PANGO_SCALE;
                            }
                        } else {
                            xx += pos[g].x_advance / (double)PANGO_SCALE;
                            yy += pos[g].y_advance / (double)PANGO_SCALE;
                            if (start_of_cluster) {
                                xx += letter_spacing / (double)PANGO_SCALE;
                            }
                        }
                    } else {
                        if (start_of_cluster) {
                            if (is_vertical) {
                                yy -= letter_spacing / (double)PANGO_SCALE;
                            } else {
                                xx -= letter_spacing / (double)PANGO_SCALE;
                            }
                        }
                    }

                } // Loop over glyphs.

                // Render
                Paint paint = text_item->get_style()->get_fill();
                context->set_source_rgb (paint.get_r(), paint.get_g(), paint.get_b());
                context->show_glyphs(cairo_glyphs);

                ++i_item;
            }  // Loop over hb_buffers in span

        }
        context->restore();

        context->translate(0, line_spacing);


        // =======Pango GlyphString to FreeType Paths =========
        std::cout << "\n333  Pango GlyphString to FreeType Paths" << std::endl;
        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            context->set_source_rgb(0.0, 0.0, 0.0);

            bool is_opposite = false;
            bool is_first_cluster = true; // Used to calculate buffer advance (correct for letter spacing).

            double buffer_advance_x = 0.0;
            int i_item = 0;
            int max_item = _text_items.size();
            for (auto text_item : _text_items) {
                auto glyphstring = text_item->_pango_glyph_string;

                auto style = text_item->_span->get_style();
                double font_size      = style->get_font_size();
                double letter_spacing = style->get_letter_spacing() * PANGO_SCALE;
                double word_spacing   = style->get_word_spacing() * PANGO_SCALE;

                context->scale(font_size, -font_size); // Glyphs are extracted from FreeType with unit font size.

                Paint paint = text_item->get_style()->get_fill();
                context->set_source_rgb (paint.get_r(), paint.get_g(), paint.get_b());

                // No font substitution is done! We need to use correct font, which we can get from the Pango::Item
                // used to create the Pango::GlyphString.
                auto pango_item = text_item->_pango_item;

                int gravity = pango_item->get_analysis().gobj()->gravity;
                std::cout << "333  Gravity: " << gravity_to_string(gravity) << std::endl;
      
                // To access glyph paths.
                auto pango_font = pango_item->get_analysis().get_font();
                FontInstance* font_instance =
                    FontFactory::get_instance().get_font_instance(pango_font->describe());

                hb_font_t* hb_font = pango_font_get_hb_font(pango_font->gobj());

                // For right-to-left text we need to reverse the order of drawing the glyphs.
                // We need to move to the end of the "buffer advance" and work backwards.
                // We calculate the buffer advance here.
                int level = pango_item->get_analysis().get_level();
                bool is_rtl = (level & 1); // Odd is rtl. (Should include gravity!)
                if ( is_rtl && _base_direction == Pango::DIRECTION_LTR ||
                     !is_rtl && _base_direction == Pango::DIRECTION_RTL) {
                    std::cout << "  Opposite direction   <--------- " << std::endl;

                    if (!is_opposite) {
                        std::cout << "    setting is_opposite true" << std::endl;
                        is_opposite = true;

                        // Find buffer advance:
                        double buffer_advance = 0;
                        int j_item = i_item;
                        while (j_item < max_item && level == _text_items[j_item]->_pango_item->get_analysis().get_level()) {
                            // Find advance for one item/glyph string.
                            // for (auto glyph_info : span->_glyph_strings[j_item].get_glyphs()) {
                            for (auto glyph_info : _text_items[j_item]->_pango_glyph_string.get_glyphs()) {
                                Pango::GlyphGeometry glyph_geom = glyph_info.get_geometry(); // Width, x_offset, y_offset.
                                double width = glyph_geom.get_width();
                                buffer_advance += width;
                            }

                            buffer_advance += letter_spacing * _text_items[j_item]->get_letter_count();
                            buffer_advance += word_spacing   * _text_items[j_item]->get_word_count();

                            if (is_first_cluster) {
                                is_first_cluster = false;
                                std::cout << "111 subtracting letter/word spacing" << std::endl;
                                buffer_advance -= letter_spacing;
                                buffer_advance -= word_spacing;
                            }

                            ++j_item;
                        }
                        buffer_advance /= ((double)PANGO_SCALE * font_size);
                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance
                                  << std::endl;

                        buffer_advance_x = buffer_advance * (is_rtl ? 1.0 : -1.0);
                        context->translate (buffer_advance_x, 0);
                    } // is opposite

                } else {

                    if (is_opposite) {
                        std::cout << "    setting is_opposite false" << std::endl;
                        is_opposite = false;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance_x
                                  << std::endl;

                        context->translate (buffer_advance_x, 0);

                        buffer_advance_x = 0.0;
                    }
                }

                int hb_x_scale = 0;
                int hb_y_scale = 0;
                hb_font_get_scale (hb_font, &hb_x_scale, &hb_y_scale);
                std::cout << "333  hb_y_scale: " << hb_y_scale << std::endl;

                for (auto glyph_info : glyphstring.get_glyphs()) {
                    Pango::Glyph         glyph      = glyph_info.get_glyph(); // typedef PangoGlyph  guint32
                    Pango::GlyphGeometry glyph_geom = glyph_info.get_geometry(); // Width, x_offset, y_offset.
                    Pango::GlyphVisAttr  glyph_attr = glyph_info.get_attr(); // typedef PangoGlyphVisAttr (is_cluster_start)

                    double width = glyph_geom.get_width()   /(double)PANGO_SCALE/font_size;
                    double dx    = glyph_geom.get_x_offset()/(double)PANGO_SCALE/font_size;
                    double dy    = glyph_geom.get_y_offset()/(double)PANGO_SCALE/font_size;

                    // Undo HarfBuzz -> Pango shift.
                    if (pango_version_check(1,48,4) != nullptr && pango_version_check(1,48,1) == nullptr) {
                        // Pango is 1.48.1 through 1.48.3.
                        if (is_vertical && gravity == Pango::GRAVITY_EAST) {
                            hb_position_t x_origin = 0.0;
                            hb_position_t y_origin = 0.0;
                            hb_font_get_glyph_v_origin (hb_font, glyph, &x_origin, &y_origin);
                            std::cout << "333  vertical:"
                                      << " x_origin: " << std::setw(6) << x_origin / (double)hb_x_scale
                                      << " y_origin: " << std::setw(6) << y_origin / (double)hb_y_scale
                                      << " dx: "       << std::setw(6) << dx
                                      << " dy: "       << std::setw(6) << dy << std::endl;
                            dx += y_origin / (double)hb_y_scale;
                            dy += x_origin / (double)hb_x_scale;
                        }
                    }

                    PangoRectangle ink_rect;
                    PangoRectangle logical_rect;
                    pango_font_get_glyph_extents(pango_font->gobj(), glyph, &ink_rect, &logical_rect);
                    double rise  = (logical_rect.y + logical_rect.height/2.0) / (double)PANGO_SCALE/font_size;

                    if (is_rtl) {
                        context->translate(-glyph_geom.get_width()/(double)PANGO_SCALE/font_size, 0);
                    }

                    context->save();
                    {
                        // Position glyph
                        if (is_vertical) {
                            switch (gravity) {
                                case PANGO_GRAVITY_EAST:
                                    if (pango_version_check(1,48,1) == nullptr) { // Pango is 1.48.1 or later.
                                        context->translate (dx, -dy);
                                    } else {
                                        context->translate (width - dx, -dy);
                                    }
                                    break;
                                case PANGO_GRAVITY_SOUTH:
                                    context->translate (dx, -dy);
                                    context->translate (0, rise);
                                    break;
                                default:
                                    std::cout << "UNHANDLED GRAVITY!" << std::endl;
                            }
                        } else {
                            context->translate(dx, -dy);
                        }

                        auto path = font_instance->get_cairo_path_for_glyph((guint32)glyph);
                        context->append_path(*path);
                        context->fill();
                    }
                    context->restore();

                    // Move onto next glyph (glyph cluster).
                    if (!is_rtl) {
                        context->translate(glyph_geom.get_width()/(double)PANGO_SCALE/font_size, 0);
                        // Apply letter & word spacing.
                        if (true) {
                            context->translate(letter_spacing/(double)PANGO_SCALE/font_size, 0);
                        }
                    } else {
                        if (true) {
                            context->translate(-letter_spacing/(double)PANGO_SCALE/font_size, 0);
                        }
                    }
                } // Loop over glyphs

                context->scale (1.0/font_size, -1.0/font_size); // Font scaling

                ++i_item;
            }
        }
        context->restore();

        context->translate(0, line_spacing);


        // ============= HarfBuzz shaping, clusters, cairo_show_glyph_string  ===========
        std::cout << "\n444  HarfBuzz shaping, clusters, cairo_show_glyph_string" << std::endl;
        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            if (is_vertical) {
                // We draw vertical text sideways to match how Inkscape (and Pango) handle vertical text.
                context->rotate(-M_PI/2.0);
            }
            context->set_source_rgb(0.0, 0.0, 0.0);

            double xx = 0.0; // Horizontal direction before rotation
            double yy = 0.0;

            bool is_opposite = false;
            bool is_first_cluster = true; // Used for calculating buffer advance.

            double buffer_advance_x = 0.0;
            double buffer_advance_y = 0.0;
            int i_item = 0;
            int max_item = _text_items.size();
            std::cout << "444  Number of TextItem's: " << max_item << std::endl;
            for (auto text_item : _text_items) {

                auto style = text_item->_span->get_style();
                double font_size      = style->get_font_size();
                double letter_spacing = style->get_letter_spacing();
                double word_spacing   = style->get_word_spacing();

                Pango::Item* pango_item = text_item->get_pango_item();
                std::cout << "444  |" << pango_item->get_segment(_text) << "|" << std::endl;

                int gravity = pango_item->get_analysis().gobj()->gravity;
                std::cout << "444  Gravity: " << gravity_to_string(gravity) << std::endl;

                auto pango_font = pango_item->get_analysis().get_font();
                FontInstance* font_instance =
                    FontFactory::get_instance().get_font_instance(pango_font->describe());
                font_instance->set_cairo_font (context, is_vertical, font_size);
                auto hb_font = pango_font_get_hb_font(pango_font->gobj());

                double rise = 0.0;
                hb_font_extents_t hb_font_extents;
                if (hb_font_get_h_extents (hb_font, &hb_font_extents)) {
                    rise = -(hb_font_extents.ascender + hb_font_extents.descender) / 2.0 / (double)PANGO_SCALE;
                    std::cout << "444  horizontal hb_font_extents: "
                              << " ascender: "  << hb_font_extents.ascender  / (double)PANGO_SCALE
                              << " descender: " << hb_font_extents.descender / (double)PANGO_SCALE
                              << " line gap: "  << hb_font_extents.line_gap  / (double)PANGO_SCALE
                              << " rise: "      << rise
                              << std::endl;
                } else {
                    std::cout << "444  Did not find horizontal font extents" << std::endl;
                }

                bool has_vertical_metrics = false;
                if (is_vertical) {
                    hb_font_extents_t hb_font_extents;
                    if (hb_font_get_v_extents (hb_font, &hb_font_extents)) {
                        std::cout << "444  vertical  hb_font_extents: "
                                  << " ascender: "  << hb_font_extents.ascender  / (double)PANGO_SCALE
                                  << " descender: " << hb_font_extents.descender / (double)PANGO_SCALE
                                  << " line gap: "  << hb_font_extents.line_gap  / (double)PANGO_SCALE
                                  << std::endl;
                        has_vertical_metrics = true;
                    } else {
                        std::cout << "444  Did not find vertical font extents" << std::endl;
                    }
                }

                // For right-to-left text we need to reverse the order of drawing the glyphs.
                // We need to move to the end of the "buffer advance" and work backwards.
                // We calculate the buffer advance here.
                int level = pango_item->get_analysis().get_level();
                bool is_rtl = (level & 1); // Odd is rtl. (Should include gravity!)

                Layout* layout = text_item->_layout;

                if ( is_rtl && layout->_base_direction == Pango::DIRECTION_LTR ||
                     !is_rtl && layout->_base_direction == Pango::DIRECTION_RTL) {
                    std::cout << "  Opposite direction   <--------- " << std::endl;

                    if (!is_opposite) {
                        std::cout << "    setting is_opposite true" << std::endl;
                        is_opposite = true;

                        // Find buffer advance:
                        int j_item = i_item;
                        while (j_item < max_item && level == _text_items[j_item]->_pango_item->get_analysis().get_level()) {
                            auto text_item_j = _text_items[j_item];
                            if (is_vertical) {
                                int j_gravity = text_item_j->_pango_item->get_analysis().gobj()->gravity;
                                switch (j_gravity) {
                                    case PANGO_GRAVITY_EAST:
                                        buffer_advance_x += text_item_j->get_x_advance();
                                        buffer_advance_y -= text_item_j->get_y_advance();
                                        break;
                                    case PANGO_GRAVITY_SOUTH:
                                        buffer_advance_x += text_item_j->get_y_advance();
                                        buffer_advance_y += text_item_j->get_x_advance();
                                        break;
                                    default:
                                        std::cout << "UNHANDLED GRAVITY" << std::endl;
                                }

                                buffer_advance_y += letter_spacing * text_item_j->get_letter_count();
                                buffer_advance_y += word_spacing   * text_item_j->get_word_count();
                                if (is_first_cluster) {
                                    buffer_advance_y -= letter_spacing;
                                    buffer_advance_y -= word_spacing;
                                    is_first_cluster = false;
                                }
                            } else {
                                buffer_advance_x += text_item_j->get_x_advance();
                                buffer_advance_y += text_item_j->get_y_advance();

                                buffer_advance_x += letter_spacing * text_item_j->get_letter_count();
                                buffer_advance_x += word_spacing   * text_item_j->get_word_count();
                                if (is_first_cluster) {
                                    is_first_cluster = false;
                                    buffer_advance_x -= letter_spacing;
                                    buffer_advance_x -= word_spacing;
                                }
                            }
                            ++j_item;
                        }
                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance_x
                                  << "  y: " << std::setw(8) << buffer_advance_y
                                  << std::endl;

                        xx += buffer_advance_x * (is_rtl ? 1 : -1);
                        yy += buffer_advance_y * (is_rtl ? 1 : -1);
                    } // is opposite


                } else {
                    if (is_opposite) {
                        std::cout << "    setting is_opposite false" << std::endl;
                        is_opposite = false;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance_x
                                  << "  y: " << std::setw(8) << buffer_advance_y
                                  << std::endl;

                        xx -= buffer_advance_x * (is_rtl ? 1 : -1);
                        yy -= buffer_advance_y * (is_rtl ? 1 : -1);

                        buffer_advance_x = 0.0;
                        buffer_advance_y = 0.0;
                    }

                }

                // Fill cairo_glyphs
                std::vector<Cairo::Glyph> cairo_glyphs;

                std::cout << "444  Text item: clusters: " << text_item->_clusters.size() << std::endl;
                for (auto& cluster : text_item->_clusters) {
                    std::cout << "444  Cluster: characters: " << cluster.get_index_characters().size()
                              << "  glyphs: " << cluster.get_index_glyphs().size()
                              << std::endl;


                    // Correct cluster position:
                    double cluster_offset = 0.0;
                    if (is_vertical && gravity == PANGO_GRAVITY_EAST && !has_vertical_metrics) {
                        // If a font does not have vertical metrics, the glyphs
                        // are first centered vertically with in the grid
                        // (determined by the vertical advances). Harfbuzz shaping
                        // then moves via 'y_offset' the ink rectangle to the top
                        // of the grid. This is less than ideal. We "correct" this
                        // by shifting the glyphs down so that the "em" box is
                        // aligned with the grid (assuming uniform vertical
                        // advances). This is done cluster-by-cluster.
                        assert (cluster.get_glyphs().size() > 0);
                        hb_glyph_info_t* glyph_info = cluster.get_glyphs()[0].get_hb_glyph_info();
                        hb_glyph_extents_t glyph_extents;
                        if (hb_font_get_glyph_extents (hb_font, glyph_info->codepoint, &glyph_extents)) {
                            double baseline_adjust =
                                font_instance->get_baseline(BASELINE_TEXT_BEFORE_EDGE) -
                                font_instance->get_baseline(BASELINE_ALPHABETIC);
                            cluster_offset =
                                (glyph_extents.y_bearing / (double)PANGO_SCALE) -
                                (baseline_adjust * font_size);
                            std::cout << "444 Cluster offset: " << cluster_offset << "  (baseline_adjust: " << baseline_adjust << ")" << std::endl;
                        } else {
                            std::cerr << "444 Retrieving glyph extents failed!" << std::endl;
                        }

                    }

                    for (auto& glyph : cluster.get_glyphs()) {

                        cairo_glyph_t cairo_glyph;

                        hb_glyph_info_t* glyph_info = glyph.get_hb_glyph_info();
                        cairo_glyph.index = glyph_info->codepoint;

                        hb_glyph_position_t * glyph_position = glyph.get_hb_glyph_position();
                        double x_offset  = glyph_position->x_offset  / (double)PANGO_SCALE;
                        double y_offset  = glyph_position->y_offset  / (double)PANGO_SCALE;
                        double x_advance = glyph_position->x_advance / (double)PANGO_SCALE;
                        double y_advance = glyph_position->y_advance / (double)PANGO_SCALE;

                        if (pango_version_check(1,48,4) != nullptr && pango_version_check(1,48,1) == nullptr) {
                            // Pango is 1.48.1 through 1.48.3.
                            if (is_vertical && gravity == PANGO_GRAVITY_EAST && has_vertical_metrics) {
                                // We've loaded the glyphs assuming vertical metrics but Harfbuzz has
                                // shaped them assuming no vertical metrics. We must "uncorrect".
                                hb_position_t x_origin = 0.0;
                                hb_position_t y_origin = 0.0;
                                hb_font_get_glyph_v_origin (hb_font, glyph_info->codepoint, &x_origin, &y_origin);
                                std::cout << "444 glyph origin: "
                                          << x_origin / (double)PANGO_SCALE << ", "
                                          << y_origin / (double)PANGO_SCALE << std::endl;
                                x_offset += x_origin / (double)PANGO_SCALE;
                                y_offset += y_origin / (double)PANGO_SCALE;
                            }
                        }

                        y_offset += cluster_offset; // Use same cluster offset for all glyphs in cluster.

                        if (is_rtl) {
                            // Position next glyph (moving backwards).
                            if (is_vertical) {
                                switch (gravity) {
                                    case PANGO_GRAVITY_EAST:
                                        xx -= x_advance;
                                        yy += y_advance;
                                        break;
                                    case PANGO_GRAVITY_SOUTH:
                                        xx -= y_advance;
                                        yy -= x_advance;
                                        break;
                                    default:
                                        std::cout << "UNHANDLED GRAVITY!" << std::endl;
                                }
            
                            } else {
                                xx -= x_advance;
                                yy -= y_advance;
                            }
                        }

                        // Add offsets.
                        if (is_vertical) {
                            switch (gravity) {
                                case PANGO_GRAVITY_EAST:
                                    cairo_glyph.x = xx + x_offset;
                                    cairo_glyph.y = yy - y_offset;
                                    break;
                                case PANGO_GRAVITY_SOUTH:
                                    cairo_glyph.x = xx + y_offset;
                                    cairo_glyph.y = yy + x_offset;
                                    // Baseline shift
                                    cairo_glyph.x += rise;
                                    break;
                                default:
                                    std::cout << "UNHANDLED GRAVITY!" << std::endl;
                            }
                        } else {
                            cairo_glyph.x = xx + x_offset;
                            cairo_glyph.y = yy - y_offset;
                        }

                        char glyph_name[32];
                        hb_font_get_glyph_name (hb_font, glyph_info->codepoint, glyph_name, sizeof (glyph_name));

                        std::cout << "444    Glyph: " << std::setw(5) << glyph_info->codepoint
                                  << "  Cairo"
                                  << "  x: " << std::setw(8) << cairo_glyph.x
                                  << "  y: " << std::setw(8) << cairo_glyph.y
                                  << "  "                    << glyph_name
                                  << std::endl;

                        // Fill cairo_glyph
                        cairo_glyphs.push_back(cairo_glyph);

                        if (!is_rtl) {
                            // Move onto next glyph
                            if (is_vertical) {
                                switch (gravity) {
                                    case PANGO_GRAVITY_EAST:
                                        xx += x_advance;
                                        yy -= y_advance;
                                        break;
                                    case PANGO_GRAVITY_SOUTH:
                                        xx += y_advance;
                                        yy += x_advance;
                                        break;
                                    default:
                                        std::cout << "UNHANDLED GRAVITY!" << std::endl;
                                }
            
                            } else {
                                xx += x_advance;
                                yy += y_advance;
                            }
                        }
                    } // Loop over glyphs.

                    if (!is_rtl) {
                        if (is_vertical) {
                            yy += letter_spacing;
                            if (cluster.is_word_end() != 0.0) {
                                yy += word_spacing;
                            }
                        } else {
                            xx += letter_spacing;
                            if (cluster.is_word_end() != 0.0) {
                                xx += word_spacing;
                            }
                        }
                    } else {
                        if (is_vertical) {
                            yy -= letter_spacing;
                            if (cluster.is_word_end() != 0.0) {
                                yy -= word_spacing;
                            }
                        } else {
                            xx -= letter_spacing;
                            if (cluster.is_word_end() != 0.0) {
                                xx -= word_spacing;
                            }
                        }
                    }
                } // Loop over clusters.

                // Render
                Paint paint = text_item->get_style()->get_fill();
                context->set_source_rgb (paint.get_r(), paint.get_g(), paint.get_b());
                context->show_glyphs(cairo_glyphs);

                ++i_item;

            } // Loop over text_items.
        }
        context->restore();

        context->translate(0, line_spacing);


        // ============= Pango GlyphString with pango_cairo_show_glyph_string ===============
        std::cout << "\n555  Pango GlyphString to pango_cairo_show_glyph_string" << std::endl;
        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            context->set_source_rgb(0.0, 0.0, 0.0);


            bool is_opposite = false;
            bool is_first_cluster = true;

            double buffer_advance = 0.0;
            int i_item = 0;
            int max_item = _text_items.size();
            for (auto text_item : _text_items) {
                auto glyphstring = text_item->_pango_glyph_string;

                auto style = text_item->_span->get_style();
                double font_size      = style->get_font_size();
                double letter_spacing = style->get_letter_spacing() * PANGO_SCALE;
                double word_spacing   = style->get_word_spacing() * PANGO_SCALE;

                // No font substitution is done! We need to use correct font, which we can get from the Pango::Item
                // used to create the Pango::GlyphString.
                auto pango_item = text_item->_pango_item;

                // Unreverse glyphs!
                if (pango_item->get_analysis().get_level() & 1) { // Odd is rtl.
                    reverse_glyph_string(glyphstring.gobj()); // TO FIX
                }

                auto pango_font = pango_item->get_analysis().get_font();
                std::cout << "555  Font: " << pango_font->describe().to_string() << std::endl;

                int gravity = pango_item->get_analysis().gobj()->gravity;
                bool center = pango_item->get_analysis().gobj()->flags & PANGO_ANALYSIS_FLAG_CENTERED_BASELINE;
                std::cout << "555  Gravity: " << gravity_to_string(gravity)
                          << "  Pango analysis center flag: " << std::boolalpha << center << std::endl;
                std::cout << "555 Glyph string Width: " << glyphstring.get_width() / (double)PANGO_SCALE << std::endl;
                if (is_vertical != center) {
                    std::cout << "555  ######## is_vertical != center" << std::endl;
                }

                hb_font_t* hb_font = pango_font_get_hb_font(pango_font->gobj());

                auto new_glyphstring = glyphstring; // Don't change original glyphstring.
                auto c_glyphstring = new_glyphstring.gobj(); // Need to use C API.
                int num_glyphs = c_glyphstring->num_glyphs;
                if (is_vertical && gravity == PANGO_GRAVITY_EAST && pango_version_check(1, 48, 1) != nullptr) {
                    // Fix glyph position ("corrected" in Pango 1.48.1).
                    // Leaves ALL glyphs, including anchored glyphs, vertically centered in em-box if font
                    // does not have vertical metrics.

                    std::cout << "555  Fixing vertical upright glyph positions!" << std::endl;

                    for (int i = 0; i < num_glyphs; ++i) {

                        int glyph_id = c_glyphstring->glyphs[i].glyph;
                        int x_offset = c_glyphstring->glyphs[i].geometry.x_offset; // Inline direction
                        int y_offset = c_glyphstring->glyphs[i].geometry.y_offset;
                        int width    = c_glyphstring->glyphs[i].geometry.width;

                        // Fix sign mistake in Pango pre 1.48.1.
                        c_glyphstring->glyphs[i].geometry.x_offset *= -1;
                        c_glyphstring->glyphs[i].geometry.x_offset += width;

                        // Correct for Cairo font glyph shift with vertical layout. Harfbuzz assumes horizontal
                        // layout but Pango loads Cairo font with vertical layout flag.
                        // (Origin is independent of setting OpenType or FreeType functions in HarfBuzz.)
                        hb_position_t x_origin = 0.0;
                        hb_position_t y_origin = 0.0;
                        hb_font_get_glyph_v_origin (hb_font, glyph_id, &x_origin, &y_origin);

                        c_glyphstring->glyphs[i].geometry.x_offset -= y_origin; // Inline direction
                        c_glyphstring->glyphs[i].geometry.y_offset -= x_origin;

                    }   // Loop over glyphs.
                }

                int level = pango_item->get_analysis().get_level();
                bool is_rtl = (level & 1); // Odd is rtl. (Should include gravity!)

                // Add letter spacing and word spacing
                double shift = 0.0;
                for (int i = 0; i < num_glyphs; ++i) {
                    if (true) {
                        c_glyphstring->glyphs[i].geometry.x_offset += shift;
                        shift += ((is_rtl) ? -1 : 1) * letter_spacing;
                    }
                }

                // For right-to-left text we need to reverse the order of drawing the glyphs.
                // We need to move to the end of the "buffer advance" and work backwards.
                // We calculate the buffer advance here.
                if ( is_rtl && _base_direction == Pango::DIRECTION_LTR ||
                     !is_rtl && _base_direction == Pango::DIRECTION_RTL) {
                    std::cout << "  Opposite direction   <--------- " << std::endl;

                    if (!is_opposite) {
                        std::cout << "    setting is_opposite true" << std::endl;
                        is_opposite = true;

                        // Find buffer advance:
                        int j_item = i_item;
                        while (j_item < max_item && level == _text_items[j_item]->_pango_item->get_analysis().get_level()) {
                            // Find advance for one item/glyph string.
                            std::cout << "   adding to buffer advance: "
                                      << _text_items[j_item]->_pango_glyph_string.get_width();
                            buffer_advance += _text_items[j_item]->_pango_glyph_string.get_width();

                            buffer_advance += letter_spacing * _text_items[j_item]->get_letter_count();
                            buffer_advance += word_spacing   * _text_items[j_item]->get_word_count();

                            if (is_first_cluster) {
                                is_first_cluster = false;
                                std::cout << "111 subtracting letter/word spacing" << std::endl;
                                buffer_advance -= letter_spacing;
                                buffer_advance -= word_spacing;
                            }

                            ++j_item;
                        }
                        buffer_advance /= (double)PANGO_SCALE;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance
                                  << std::endl;

                        std::cout << "   Translating: " << buffer_advance * (is_rtl?1:-1) << std::endl; 
                        context->translate (buffer_advance * (is_rtl?1:-1), 0);
                    } // is opposite


                } else {
                    std::cout << "  Not Opposite direction   <--------- " << std::endl;

                    if (is_opposite) {
                        std::cout << "    setting is_opposite false" << std::endl;
                        is_opposite = false;

                        std::cout << "   Buffer advance: "
                                  << "  x: " << std::setw(8) << buffer_advance
                                  << std::endl;

                        std::cout << "   Translating: " << buffer_advance * (is_rtl?1:-1, 1) << std::endl; 
                        context->translate (buffer_advance * (is_rtl?-1:1), 0);
                        buffer_advance = 0.0;
                    }
                }

                double string_width = (double)(glyphstring.get_width()+shift)/PANGO_SCALE;
        
                if (is_rtl) {
                    std::cout << "555  Translating string width (before):" << -string_width << std::endl; 
                    context->translate(-string_width, 0);
                }

                context->save(); // Shift baseline for sideways text.

                if (is_vertical && gravity == PANGO_GRAVITY_SOUTH) {
                    // Sideways text in "mixed" glyph orientation.
                    double rise = 0.0;
                    hb_font_extents_t hb_font_extents;
                    if (hb_font_get_h_extents (hb_font, &hb_font_extents)) {
                        rise = -(hb_font_extents.ascender + hb_font_extents.descender) / 2.0 / (double)PANGO_SCALE;
                    }
                    context->translate(0, -rise);
                }

                Paint paint = text_item->get_style()->get_fill();
                context->set_source_rgb (paint.get_r(), paint.get_g(), paint.get_b());
                pango_cairo_show_glyph_string(context->cobj(), pango_font->gobj(), c_glyphstring);
                // pango_cairo_show_glyph_string(context->cobj(), pango_font->gobj(), glyphstring.gobj());

                context->restore(); // Shift for sideways text.

                if (!is_rtl) {
                    std::cout << "555  Translating string width (after): " << string_width << std::endl; 
                    context->translate(string_width, 0);
                }

                ++i_item;
            }  // Loop over glyphstrings.
        }
        context->restore();

        // goto SKIP2;
        context->translate(0, line_spacing);

        // ============= Use Pango Layout ==============
        std::cout << "\n666  Using Pango Layout  pango_layout" << std::endl;

        // In pango_renderer_draw_layout_line():
        // if (PangoItem->analysis.flags & PANGO_ANALYSIS_FLAG_CENTERED_BASELINE), shift y by rise.
        // rise = PANGO_ATTR_RISE->value + logical_rect.y + logical_rect.height/2
        // where PANGO_ATTR_RISE is an item->analysis.extra_attrs that handles baseline displacement.

        // Note: vertical baseline depends on first character in line.
        context->save();
        {
            context->translate(x, y);

            if (_text_items.size() > 0) {
                double font_size = _text_items[0]->_span->get_style()->get_font_size();
                draw_square(context, font_size, _base_direction == Pango::DIRECTION_RTL);
            } else {
                std::cerr << "  NO TEXT!" << std::endl;
            }

            context->set_source_rgb(0.0, 0.0, 0.0);

            if (_text_items.size() > 0) {
                auto pango_item = _text_items[0]->_pango_item;
                auto pango_font = pango_item->get_analysis().get_font();
                auto font_metrics = pango_font->get_metrics();
                auto ascent = font_metrics.get_ascent() / PANGO_SCALE;
                std::cout << "666  Ascent: " << ascent << std::endl;
                context->translate(0, -ascent); // Reference point for Layout is top of line box. Should be ascent.
            }

            PangoLayout *layout = pango_cairo_create_layout(context->cobj());
            pango_layout_set_auto_dir (layout, false); // Use base direction, don't guess from first letter.
            pango_layout_set_text (layout, _text.c_str(), -1);
            pango_layout_set_attributes (layout, _attrs.gobj());

            // Using Pango::Attribute's to set base gravity, gravity_hint, and base_direction does not
            // seem to work on outer most block. Setting via Pango::Context does seem to work.
            auto pango_context = pango_layout_get_context(layout);
            std::cout << "666 Pango context: "
                      << " base gravity: " << gravity_to_string      (pango_context_get_base_gravity(pango_context))
                      << " gravity hint: " << gravity_hint_to_string (pango_context_get_gravity_hint(pango_context))
                      << std::endl;
            pango_context_set_base_gravity (pango_context, (PangoGravity)_base_gravity);
            pango_context_set_gravity_hint (pango_context, (PangoGravityHint)_gravity_hint);
            pango_context_set_base_dir     (pango_context, (PangoDirection)_base_direction);

            PangoAttrList* attrs = pango_layout_get_attributes (layout);
            auto attrs2 = Glib::wrap(attrs);
            dump_attributes(attrs2, "666");

            if (_base_direction == Pango::DIRECTION_RTL) {
                int width = 0;
                int height = 0;
                pango_layout_get_size(layout, &width, &height);
                context->translate(-width/(double)PANGO_SCALE, 0);
            }

            pango_cairo_show_layout (context->cobj(), layout);

        }
        context->restore();

        y += 2 * line_spacing;

    } // Loop over spans

}


Layout::Span::Span(Layout& l, const Glib::ustring& text, SPStyle* style,
                   boost::optional<SVGLength> x,
                   boost::optional<SVGLength> y,
                   boost::optional<SVGLength> dx,
                   boost::optional<SVGLength> dy,
                   boost::optional<SVGLength> rotate)
    : _style(*style),
      _x(x),
      _y(y),
      _dx(dx),
      _dy(dy),
      _rotate(rotate)
{
    _start_index_char = l._text.size();
    std::cout << "Span: start_index_char: " << _start_index_char << "  " << text << std::endl;
    _start_index_byte = l._text.bytes();
    _length_char = text.size();
    _length_byte = text.bytes();
    l._text += text;

    // Attributes that effect glyph selection. TODO: add language, variant, gravity.
    // We'll handle cosmetic properties ourselves (fill, stroke, etc.).
    // We'll handle positioning glyphs ourselves (baseline, letter spacing(?) , etc.).
    // Note: Pango treats font size as in points, we need in pixels. (Depends on which font map is used!)
    _font_description = Pango::FontDescription(style->get_font_family() + " "
                                               + std::to_string(0.75 * style->get_font_size()));

    auto attr_description = Pango::AttrFontDesc::create_attr_font_desc(_font_description);
    attr_description.set_start_index(_start_index_byte);
    attr_description.set_end_index(_start_index_byte + _length_byte);
    l._attrs.change(attr_description);

    // base_gravity and gravity_hint do not seem to work on outermost block.
    // Need to set on Pango context directly.
    if (style->get_base_gravity() != Pango::GRAVITY_AUTO) {  // Will abort if auto, see Pango documentation.
        auto attr_base_gravity = Pango::AttrInt::create_attr_gravity(style->get_base_gravity());
        attr_base_gravity.set_start_index(_start_index_byte);
        attr_base_gravity.set_end_index(_start_index_byte + _length_byte);
        l._attrs.change(attr_base_gravity);
    }

    auto attr_gravity_hint  = Pango::AttrInt::create_attr_gravity_hint(style->get_gravity_hint());
    attr_gravity_hint.set_start_index(_start_index_byte);
    attr_gravity_hint.set_end_index(_start_index_byte + _length_byte);
    l._attrs.change(attr_gravity_hint);

    // Setting features here isn't enough as we are not shaping via Pango but using HarfBuzz directly.
    // We need to pass the features on to HarfBuzz when shaping.
    if (style->get_font_features().length() > 0) {
        auto attr_font_features = Pango::AttrString::create_attr_font_features(style->get_font_features());
        attr_font_features.set_start_index(_start_index_byte);
        attr_font_features.set_end_index(_start_index_byte + _length_byte);
        l._attrs.change(attr_font_features);
    }

    // This is used by Pango::Layout but not by shaping... so it's useless for us.
    // Pango letter-spacing also puts space half before/half after character.
    // see pango_glyph_item_letter_space().
    if (style->get_letter_spacing() > 0) {
        auto attr_letter_spacing =
            Pango::Attribute::create_attr_letter_spacing((int)(style->get_letter_spacing() * PANGO_SCALE));
        attr_letter_spacing.set_start_index(_start_index_byte);
        attr_letter_spacing.set_end_index(_start_index_byte + _length_byte);
        l._attrs.change(attr_letter_spacing);
    }

    auto fill = style->get_fill();
    auto attr_foreground =
        Pango::Attribute::create_attr_foreground(fill.get_r16(), fill.get_g16(), fill.get_b16());
    attr_foreground.set_start_index(_start_index_byte);
    attr_foreground.set_end_index(_start_index_byte + _length_byte);
    l._attrs.change(attr_foreground);
}

Layout::Glyph::Glyph(hb_glyph_info_t* hb_glyph_info, hb_glyph_position_t* hb_glyph_position)
    : _hb_glyph_info(hb_glyph_info)          // Layout retains ownership.
    , _hb_glyph_position(hb_glyph_position)  // Layout retains ownership.
{}

Layout::TextItem::TextItem (Pango::Item* pango_item, Span* span, int index_char, Layout* layout)
    : _pango_item (pango_item)
    , _span (span)
    , _index_char (index_char)
    , _layout (layout)
{
    std::cout << "\nTextItem::TextItem: Entrance" << std::endl;

    // *************** PANGO *****************
    // TEMP TEMP TEMP  WE WILL USE HARFBUZZ SHAPING

    // Use Pango C API (C++ API currently missing shape_full!)
    PangoGlyphString* glyph_string_c = pango_glyph_string_new();
    PangoItem* pango_item_c = pango_item->gobj();
    pango_shape_full (layout->_text.c_str() + pango_item->get_offset(), pango_item->get_length(),
                      layout->_text.c_str(), -1, &pango_item_c->analysis, glyph_string_c);

    // Glyphs come out in visual order (left-to-right)... but we need logigical order per SVG (i.e. input order).
    // We need to reverse glyph order for right-to-left text! (This also makes later code easier.)
    // We use the C API as it allows access to the underlying data.
    if (pango_item->get_analysis().get_level() & 1) { // Odd is rtl.
        reverse_glyph_string(glyph_string_c); // TO FIX
    }

    _pango_glyph_string = std::move(Glib::wrap(glyph_string_c));

    // ************** HARFBUZZ *****************

    auto pango_font = pango_item->get_analysis().get_font();

    // See options.cc in harfbuzz code.
    hb_font_t* hb_font = pango_font_get_hb_font(pango_font->gobj());
    // hb_face_t* hb_face = hb_font_get_face (hb_font);

    // Create shaping buffer and initialize it.
    _hb_buffer = hb_buffer_create ();

    // Add text (including surrounding text).
    hb_buffer_add_utf8 (_hb_buffer, layout->_text.c_str(), -1, pango_item->get_offset(), pango_item->get_length());

    // Set buffer properties...

    // A HarfBuzz buffer has one text direction. There is no equivalent to Pango's concept of
    // gravity which can allow vertical text with mixed East Asian and Latin text to have glyphs
    // with different orientations. We use Pango gravity here to set the HarfBuzz buffer direction.

    // TODO: Study pango_hb_shape() in detail to make sure we are doing things correctly!
    int gravity = pango_item->get_analysis().gobj()->gravity; // No C++ API to get gravity!
    switch (gravity) {
        case Pango::GRAVITY_EAST:
        case Pango::GRAVITY_WEST: // Not used in practice(?).
            hb_buffer_set_direction (_hb_buffer, HB_DIRECTION_TTB);  // Only Ogham (Old Irish) uses BTT.
            std::cout << "Harfbuzz direction: TTB" << std::endl;
            break;
        case Pango::GRAVITY_SOUTH:
        case Pango::GRAVITY_NORTH: // Not used in practice(?).
            // Don't set. Let HarfBuzz guess.
            break;
        default:
            // Should never happen!
            std::cerr << "Layout::TextItem::TextItem: Unhandled gravity" << std::endl;
    }

    // Copied from pango_hb_shape()
    hb_feature_t features[32];
    unsigned int num_features = 0;
    pango_font_get_features (pango_font->gobj(), features, G_N_ELEMENTS (features), &num_features); // No C++ API
    apply_extra_attributes (pango_item_c->analysis.extra_attrs, features, G_N_ELEMENTS (features), &num_features);
    std::cout << "Features: " << num_features << std::endl;

    hb_buffer_guess_segment_properties (_hb_buffer); // Guess non-set properties.

    // Do actual shaping!
    hb_shape (hb_font, _hb_buffer, features, num_features); // font, buffer, features, num_features

    // Debug
    std::cout << " Shaping results: Start &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" << std::endl;
    _layout->dump_buffers(pango_font, _pango_glyph_string, _hb_buffer);  // TEMP TEMP TEMP
    std::cout << " Shaping results: End   &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" << std::endl;

    // ********* Fill our Cluster, Character, and Glyph vectors. **********

    // * A cluster is an indivisible presentation unit. It may consist
    //   of one or more characters corresponding to one or more glyphs.
    // * A character may be represented by one, or more glyphs.
    // * A glyph may represent a fractional part of one or more characters.
    // In the simplest case, a cluster consists of one character represented by one glyph.

    // Glyphs come out in visual order (left-to-right)... but we need logigical order per SVG (i.e. input order).
    // We need to reverse glyph order for right-to-left text! (This also makes later code easier.)
    unsigned int len = 0;
    hb_glyph_info_t *glyph_info = hb_buffer_get_glyph_infos (_hb_buffer, &len);
    assert (len > 0);

    if (glyph_info[0].cluster > glyph_info[len-1].cluster) { // Glyphs reversed (cluster is byte in text input).
        layout->dump_hb_buffer (hb_font, _hb_buffer);
        hb_buffer_reverse (_hb_buffer);
        std::cout << "Glyphs Reversed:" << std::endl;
        layout->dump_hb_buffer (hb_font, _hb_buffer);
    }
    assert (glyph_info[0].cluster <= glyph_info[len-1].cluster);

    unsigned int len_check = 0;
    hb_glyph_position_t *glyph_positions = hb_buffer_get_glyph_positions (_hb_buffer, &len_check);
    assert (len == len_check);

    Glib::ustring segment_text = pango_item->get_segment(layout->_text);
    Glib::ustring::const_iterator it_char = segment_text.begin();

    std::cout << "\nHarfBuzz clusters: --------------------------" << std::endl;
    std::cout << "  Adding " << len << " glyphs (" << segment_text << ")" << std::endl;
    unsigned int cluster_byte_old = -1;
    unsigned int cluster_byte_start = glyph_info[0].cluster;

    // Some variables so we can calculate cluster (and buffer) advance.
    double cluster_x_advance = 0.0;
    double cluster_y_advance = 0.0;

    // Loop over glyphs
    int index_char = 0;
    for (unsigned int i = 0; i < len; i++) {
        hb_codepoint_t gid   = glyph_info[i].codepoint;
        unsigned int cluster_byte = glyph_info[i].cluster;

        char glyphname[32];
        hb_font_get_glyph_name (hb_font, gid, glyphname, sizeof (glyphname));

        if (cluster_byte_old != cluster_byte) {
            // New cluster!
            cluster_byte_old = cluster_byte;
            std::cout << "\nAdding char: HB cluster: " << _clusters.size() << std::endl;
            _clusters.emplace_back(Cluster(this));

            // Add characters to cluster.

            // How many bytes do we need to fill cluster?
            int cluster_byte_end = cluster_byte_start + segment_text.bytes() ; // In case last glyph in hb_buffer.
            for (int j = i + 1; j < len; j++) {
                if (glyph_info[j].cluster != cluster_byte_old) {
                    cluster_byte_end = glyph_info[j].cluster;
                    break;
                }
            }
            std::cout << "  Adding char: "
                      << "  cluster_byte: "       << cluster_byte
                      << "  cluster_byte_start: " << cluster_byte_start
                      << "  cluster_byte_end: "   << cluster_byte_end
                      << "  segment_text.bytes: " << segment_text.bytes() << std::endl;

            int cluster_bytes_to_get = cluster_byte_end - cluster_byte;
            assert (cluster_bytes_to_get > 0);

            std::cout << "  Adding char: Need " << cluster_bytes_to_get << " bytes!" << std::endl;

            auto it_start = it_char;
            bool first_char = true;
            while (it_char.base() - it_start.base() < cluster_bytes_to_get) {
                std::cout << "  Adding char: " << *it_char
                          << " " << segment_text.substr(index_char, 1) << std::endl;
                _clusters.back().add_index_character(index_char);

                // Character in substring should match character in global string.
                assert (segment_text.substr(index_char, 1) ==
                        layout->_text.substr(_index_char + index_char, 1));

                if (first_char) {
                    first_char = false;
                    PangoLogAttr const &char_attributes = layout->_char_attributes[_index_char + index_char];
                    _clusters.back().set_log_attributes(char_attributes);
                    ++_letter_count;
                    if (char_attributes.is_word_end) {
                        ++_word_count;
                    }
                }

                ++it_char;
                ++index_char;
            }

            cluster_x_advance = 0.0;
            cluster_y_advance = 0.0;
        }

        // Add glyph
        std::cout << "  Adding glyph hb:    " << std::setw(4) << glyph_info[i].codepoint
                  << " " << std::setw(8) << (glyphname?glyphname:"null");
        _clusters.back().add_index_glyph(i);
        _clusters.back().add_glyph(Glyph(&glyph_info[i], &glyph_positions[i]));

        double glyph_x_advance = glyph_positions[i].x_advance / (double)PANGO_SCALE;
        double glyph_y_advance = glyph_positions[i].y_advance / (double)PANGO_SCALE;

        // Cluster
        cluster_x_advance += glyph_x_advance;
        cluster_y_advance += glyph_y_advance;
        
        _clusters.back().set_x_advance(cluster_x_advance);
        _clusters.back().set_y_advance(cluster_y_advance);

        std::cout << " Cluster:  "
                  << "  x_advance: " << cluster_x_advance
                  << "  y_advance: " << cluster_y_advance
                  << std::endl;

        // Buffer
        _x_advance += glyph_x_advance;
        _y_advance += glyph_y_advance;
        
    } // Glyphs

    if (it_char != segment_text.end()) {
        std::cerr << "Failed to use up all characters! ***************************" << std::endl;
    }

    std::cout << "TextItem::TextItem: Exit\n" << std::endl;
}

// ****************** DEBUG OUTPUT ******************

void
Layout::dump_spans()
{
    for (auto span : _spans) {
        auto text = std::string_view(_text.data() + span->_start_index_byte, span->_length_byte);
        SPStyle* style = span->get_style();
        std::cout << "  Span: start byte: " << std::setw(3) << span->_start_index_byte
                  << "  font-size: "     << style->get_font_size()
                  << "  font-family: "   << style->get_font_family()
                  << "  font-features: |" << style->get_font_features() << "|"
                  << "  |" << text << "|"
                  << std::endl;
    }
}

void
Layout::dump_item(const Pango::Item& pango_item)
{
    std::cout << "    item: " << pango_item.get_offset() << "-" << pango_item.get_offset() + pango_item.get_length() - 1
              << " (" << pango_item.get_num_chars() << ")"
              << "  |" << pango_item.get_segment(_text) << "|" << std::endl;
    auto analysis = pango_item.get_analysis();
    std::cout << "      Analysis: "
              << "  level: "    << (unsigned)analysis.get_level()
              << "  language: " << analysis.get_language().get_string()
              << "  font: "     << analysis.get_font()->describe().to_string()
              << "  gravity: "  << gravity_to_string(analysis.gobj()->gravity)
              << std::endl;

    auto extra_attrs = analysis.get_extra_attrs();
    for (auto extra_attr : extra_attrs) {
        switch (extra_attr.get_type()) {
            // See pango-attributes.h
            case 17:
                std::cout << "        letter_spacing: " << ((PangoAttrInt*)extra_attr.gobj())->value;
                break;
            case 21:
                std::cout << "        base_gravity: " << ((PangoAttrInt*)extra_attr.gobj())->value;
                break;
            case 22:
                std::cout << "        gravity_hint: " << ((PangoAttrInt*)extra_attr.gobj())->value;
                break;
            case 23:
                std::cout << "        features: " << ((PangoAttrFontFeatures*)extra_attr.gobj())->features;
                break;
            default:
                std::cout << "        extra_attrs: type: " << extra_attr.get_type();
        }
        std::cout  << "  start: " << extra_attr.get_start_index() << "  end: " << extra_attr.get_end_index() << std::endl;
    }
}


void
Layout::dump_hb_buffer(hb_font_t* hb_font, hb_buffer_t* hb_buffer)
{
    /* Get glyph information and positions out of the buffer. */

    std::cout << "hb_buffer:" << std::endl;

    bool is_vertical = HB_DIRECTION_IS_VERTICAL (hb_buffer_get_direction(hb_buffer));
    std::cout << "   Direction: "
              << (is_vertical ? "Vertical" : "Horizontal")
              << std::endl;

    unsigned int len = hb_buffer_get_length (hb_buffer);
    hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
    hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, NULL);

    std::cout << "   Glyph x_offset y_offset    width x_advance y_advance cluster name" << std::endl;
    std::cout << "   -----------------------------------------------------------------" << std::endl;
               
    /* Print them out as is. */
    for (unsigned int i = 0; i < len; i++) {
        hb_codepoint_t gid   = info[i].codepoint;
        unsigned int cluster = info[i].cluster;
        double x_advance = pos[i].x_advance / (double)PANGO_SCALE;
        double y_advance = pos[i].y_advance / (double)PANGO_SCALE;
        double x_offset  = pos[i].x_offset  / (double)PANGO_SCALE;
        double y_offset  = pos[i].y_offset  / (double)PANGO_SCALE;

        char glyphname[32];
        hb_font_get_glyph_name (hb_font, gid, glyphname, sizeof (glyphname));

        std::cout << "  " << std::setw(6) << gid
                  << "  " << std::setw(7) << x_offset
                  << "  " << std::setw(7) << y_offset
                  << "  " << std::setw(7) << (is_vertical ? y_advance : x_advance)

                  << "    " << std::setw(7) << x_advance
                  << "    " << std::setw(7) << y_advance
                  << "      " << std::setw(2) << cluster
                  << "  " << glyphname
                  << std::endl;
    }
}

/**
 * Dump Pango and HarfBuzz buffers. Check they agree.
 */
void
Layout::dump_buffers(const Glib::RefPtr<Pango::Font> pango_font,
                     const Pango::GlyphString& glyph_string,
                     hb_buffer_t* hb_buffer)
{
    std::cout << "\nPango vs. HarfBuzz" << std::endl;
    auto description = pango_font->describe();
    std::cout << description.to_string() << std::endl;

    bool vertical = false;
    hb_direction_t hb_direction = HB_DIRECTION_LTR;
    auto gravity = description.get_gravity();
    switch (gravity) {
        case Pango::GRAVITY_EAST:
            hb_direction = HB_DIRECTION_TTB;
            vertical = true;
            break;
        case Pango::GRAVITY_SOUTH:
            hb_direction = HB_DIRECTION_LTR;
            break;
        case Pango::GRAVITY_WEST:
            hb_direction = HB_DIRECTION_BTT;
            vertical = true;
            break;
        case Pango::GRAVITY_NORTH:
            hb_direction = HB_DIRECTION_RTL;
            break;
        default:
            std::cout << "Layout::dump_buffers: Unhandled gravity" << std::endl;
    }

    hb_font_t* hb_font = pango_font_get_hb_font(pango_font->gobj());

    hb_font_extents_t hb_font_extents;

    hb_font_get_extents_for_direction (hb_font, hb_direction, &hb_font_extents);
    std::cout << "hb_font_extents: "
              << " ascender: "  << hb_font_extents.ascender  / (double)PANGO_SCALE
              << " descender: " << hb_font_extents.descender / (double)PANGO_SCALE
              << " line gap: "  << hb_font_extents.line_gap  / (double)PANGO_SCALE
              << " direction: " << (int)hb_direction
              << std::endl;

    hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
    hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, NULL);
    
    std::cout << "         GlyphInfo                     Logicial                          Ink                          Cluster     " << std::endl;
    std::cout << "   Glyph x_offset y_offset    width       x        y   width  height       x       y   width  height  #/start Name" << std::endl;
    std::cout << "   --------------------------------------------------------------------------------------------------------------" << std::endl;

    int i = 0;
    auto log_clusters = glyph_string.gobj()->log_clusters;  // No C++ API!
    std::vector<Pango::GlyphInfo> glyph_infos = glyph_string.get_glyphs();

    for (auto& glyph_info : glyph_infos) {
        Pango::Glyph         glyph      = glyph_info.get_glyph();    // typedef PangoGlyph  guint32
        Pango::GlyphGeometry glyph_geom = glyph_info.get_geometry(); // Width, x_offset, y_offset.
        Pango::GlyphVisAttr  glyph_attr = glyph_info.get_attr();     // typedef PangoGlyphVisAttr (is_cluster_start)

        auto pango_width    = glyph_geom.get_width()     / (double)PANGO_SCALE;
        auto pango_x_offset = glyph_geom.get_x_offset()  / (double)PANGO_SCALE;
        auto pango_y_offset = glyph_geom.get_y_offset()  / (double)PANGO_SCALE;

        Pango::Rectangle ink_rect;
        Pango::Rectangle logical_rect;
        pango_font->get_glyph_extents(glyph, ink_rect, logical_rect);
        char glyphname2[32];
        hb_font_get_glyph_name (hb_font, glyph, glyphname2, sizeof (glyphname2));

        std::cout << "  " << std::setw(6) << glyph
                  << "  " << std::setw(7) << pango_x_offset
                  << "  " << std::setw(7) << pango_y_offset
                  << "  " << std::setw(7) << pango_width
               
                  << "  " << std::setw(6) << logical_rect.get_x()      / (double)PANGO_SCALE
                  << "  " << std::setw(7) << logical_rect.get_y()      / (double)PANGO_SCALE
                  << "  " << std::setw(6) << logical_rect.get_width()  / (double)PANGO_SCALE // Vertical width
                  << "  " << std::setw(6) << logical_rect.get_height() / (double)PANGO_SCALE // WRONG!!! Horizontal width + 1(!)
               
                  << "  " << std::setw(6) << ink_rect.get_x()      / (double)PANGO_SCALE
                  << "  " << std::setw(6) << ink_rect.get_y()      / (double)PANGO_SCALE
                  << "  " << std::setw(6) << ink_rect.get_width()  / (double)PANGO_SCALE  // Vertical width
                  << "  " << std::setw(6) << ink_rect.get_height() / (double)PANGO_SCALE  // WRONG!!! Horizontal width + 1(!)
                  << "  " << std::setw(3) << log_clusters[i]
                  << "  " << std::setw(2) << glyph_attr.is_cluster_start
                  << "  " << (glyphname2 ? glyphname2 : " unnamed glyph!")
                  << std::endl;


        double hb_x_advance = pos[i].x_advance / (double)PANGO_SCALE;
        double hb_y_advance = pos[i].y_advance / (double)PANGO_SCALE;
        double hb_x_offset  = pos[i].x_offset  / (double)PANGO_SCALE;
        double hb_y_offset  = pos[i].y_offset  / (double)PANGO_SCALE;

        hb_position_t x, y;
        hb_font_get_glyph_advance_for_direction (hb_font, info[i].codepoint, hb_direction, &x, &y);

        if (hb_x_advance != x / (double)PANGO_SCALE) {
            std::cout << "ERROR: HarfBuzz x advance different!!!!!! "
                      << " x_advance: " << hb_x_advance << " != " << x / (double)PANGO_SCALE
                      << std::endl;
        }
        if (hb_y_advance != y / (double)PANGO_SCALE) {
            std::cout << "ERROR: HarfBuzz y advance different!!!!!! "
                      << " y_advance: " << hb_y_advance << " != " << y / (double)PANGO_SCALE
                      << std::endl;
        }

        hb_glyph_extents_t hb_glyph_extents;
        hb_font_get_glyph_extents (hb_font, info[i].codepoint, &hb_glyph_extents);

        char glyphname[32];
        hb_font_get_glyph_name (hb_font, info[i].codepoint, glyphname, sizeof (glyphname));
        std::cout << "    hb: "
                  << "  " << std::setw(7) << hb_x_offset
                  << "  " << std::setw(7) << hb_y_offset
                  << "  " << std::setw(7) << (vertical ? hb_y_advance : hb_x_advance)

                  << "  " << std::setw(6) << "."
                  << "  " << std::setw(7) << "."
                  << "  " << std::setw(6) << "."
                  << "  " << std::setw(6) << "."

                  << "  " << std::setw(6) << hb_glyph_extents.x_bearing / (double)PANGO_SCALE
                  << "  " << std::setw(6) << hb_glyph_extents.y_bearing / (double)PANGO_SCALE
                  << "  " << std::setw(6) << hb_glyph_extents.width  / (double)PANGO_SCALE
                  << "  " << std::setw(6) << hb_glyph_extents.height / (double)PANGO_SCALE

                  << "  " << std::setw(3) << info[i].cluster
                  << "  " << " ."
                  << "  " << (glyphname ? glyphname : " unnamed glyph!")

                  << "\n"
                  << std::endl;

        // *************** Compare Pango to HarfBuzz **************
        if (info[i].codepoint != glyph) {
            std::cout << "ERROR: Pango and HarfBuzz glyphs different!!!!!!"
                      << "  Pango: " << glyph << "  HarfBuzz: " << info[i].codepoint << std::endl;
        }

        if (vertical) { // gravity == Pango::GRAVITY_EAST || gravity == Pango::GRAVITY_WEST) {
            if (pango_x_offset != hb_y_offset - hb_y_advance) {
                std::cout << "  Pango and HarfBuzz vertical x_offset disagree! " << glyph
                          << " " << pango_x_offset
                          << " " << hb_y_offset - hb_y_advance
                          << std::endl;
            }
            if (pango_y_offset != -hb_x_offset) {
                std::cout << "  Pango and HarfBuzz vertical y_offset disagree! " << glyph
                          << " " << pango_y_offset
                          << " " << - hb_x_offset
                          << std::endl;
            }
            if (pango_width != -hb_y_advance) {
                std::cout << "  Pango and HarfBuzz vertical advance disagree! " << glyph
                          << " " << pango_width
                          << " " << -hb_y_advance
                          << std::endl;
            }
        } else {
            if (pango_x_offset != hb_x_offset) {
                std::cout << "  Pango and HarfBuzz horizontal x_offset disagree! " << glyph
                          << " " << pango_x_offset
                          << " " << hb_x_offset
                          << std::endl;
            }
            if (pango_y_offset != -hb_y_offset) {
                std::cout << "  Pango and HarfBuzz horizontal y_offset disagree! " << glyph
                          << " " << pango_y_offset
                          << " " << hb_y_offset
                          << std::endl;
            }
            if (pango_width != hb_x_advance) {
                std::cout << "  Pango and HarfBuzz horizontal advance disagree! " << glyph
                          << " " << pango_width
                          << " " << hb_x_advance
                          << std::endl;
            }
        }

        ++i;
    } // Loop over glyphs
}

void dump_attribute (Pango::Attribute& attr, Glib::ustring& tag)
{
    std::cout << tag;

    switch (attr.get_type()) {
        case PANGO_ATTR_FONT_DESC:
        {
            Pango::AttrFontDesc* attr_desc = static_cast<Pango::AttrFontDesc *>(&attr);
            if (attr_desc) {
                std::cout << "  PANGO_ATTR_FONT_DESC: "
                          << attr_desc->get_desc().to_string()
                          << std::endl;
            } else {
                std::cout << " Hmm" << std::endl;
            }
            break;
        }
        case PANGO_ATTR_FOREGROUND:
        {
            std::cout << "  PANGO_ATTR_FOREGROUND" << std::endl;
            break;
        }
        case PANGO_ATTR_LETTER_SPACING:
        {
            Pango::AttrInt* attr_int = static_cast<Pango::AttrInt *>(&attr);
            if (attr_int) {
                std::cout << "  PANGO_ATTR_LETTER_SPACING: " << attr_int->get_value() <<  std::endl;
            } else {
                std::cout << " Hmm" << std::endl;
            }
            break;
        }
        case PANGO_ATTR_GRAVITY:
        {
            Pango::AttrInt* attr_int = static_cast<Pango::AttrInt *>(&attr);
            if (attr_int) {
                std::cout << "  PANGO_ATTR_GRAVITY: "
                          << gravity_to_string (attr_int->get_value()) <<  std::endl;
            } else {
                std::cout << " Hmm" << std::endl;
            }
            break;
        }
        case PANGO_ATTR_GRAVITY_HINT:
        {
            Pango::AttrInt* attr_int = static_cast<Pango::AttrInt *>(&attr);
            if (attr_int) {
                std::cout << "  PANGO_ATTR_GRAVITY_HINT: "
                          << gravity_hint_to_string (attr_int->get_value()) <<  std::endl;
            } else {
                std::cout << " Hmm" << std::endl;
            }
            break;
        }
        case PANGO_ATTR_FONT_FEATURES:
            std::cout << "  PANGO_ATTR_FONT_FEATURES" << std::endl;
            break;
        default:
            std::cout << "  Unknown: " << attr.get_type() << std::endl;
    }
}

// Dump attributes
void dump_attributes (Pango::AttrList& attrs, Glib::ustring tag)
{
    Pango::AttrIter dump_iter = attrs.get_iter();
    while (dump_iter) {
        int start = -1;
        int end = -1;
        dump_iter.get_range(start, end);
        auto desc   = dump_iter.get_font_desc();
        auto lang   = dump_iter.get_language();
        auto attrs  = dump_iter.get_attrs();
        auto extras = dump_iter.get_extra_attrs();
        std::cout << tag
                  << " Attr range: " << std::setw(3) << start << " to " << std::setw(10) << end
                  << " Description: " << desc.to_string()
                  << " Language: " << lang.get_string()
                  << " Number: " << attrs.size()
                  << " Extra: " << extras.size()
                  << std::endl;
        for (Pango::Attribute attr : attrs) {
            dump_attribute(attr, tag);
        }
        // Extras duplicate non-font regular attributes. No need to dump them.
        // std::cout << "Extra Attributes:" << std::endl;
        // for (Pango::Attribute attr : extras) {
        //     dump_attribute(attr);
        // }

        dump_iter++;
    }
}

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
