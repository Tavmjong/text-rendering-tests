
#include "layout-style.h"

#include <iostream>
#include <vector>

#include <glibmm/regex.h>

// ------ DUMMY SVG STYLING ------

Paint::Paint(Glib::ustring paint)
{
    if (paint.length() != 7) {
        std::cerr << "Paint::Paint: input of incorrect lenght: " << paint << std::endl;
        return;
    }

    if (paint[0] != '#') {
        std::cerr << "Paint::Paint: input does not begin with '#': " << paint << std::endl;
        return;
    }

    paint.erase(0, 1);
    int value = std::stoi(paint, 0, 16);
    r = ((0xff0000 & value) >> 16) / 255.0;
    g = ((0x00ff00 & value) >>  8) / 255.0;
    b = ((0x0000ff & value)      ) / 255.0;
    std::cout << "Paint::Paint:"
              << "  r: " << r
              << "  g: " << g
              << "  b: " << b
              << std::endl;
}

Glib::ustring
Paint::write() {
    Glib::ustring paint("#");
    paint += std::to_string((int)(r * 255)) + ",";
    paint += std::to_string((int)(g * 255)) + ",";
    paint += std::to_string((int)(b * 255));
    return paint;
}

void
SPStyle::parse(Glib::ustring style)
{
    std::vector<Glib::ustring> tokens = Glib::Regex::split_simple("\\s*;\\s*", style);
    for (auto token : tokens) {
        std::vector<Glib::ustring> parts = Glib::Regex::split_simple("\\s*:\\s*", token);
        if (parts.size() != 2) {
            std::cerr << "SPStyle::SPStyle: invalid property: " << token << std::endl;
            continue;
        }

        auto property = parts[0];
        auto value = parts[1];

        if (property == "font-family") {
            _font_family = value;
        } else if (property == "font-size") {
            _font_size = std::stod(value);
        } else if (property == "font-features") {
            _font_features = value;
        } else if (property == "text-orientation") {
            if (value == "mixed") {
                _base_gravity = Pango::GRAVITY_EAST;
                _gravity_hint = Pango::GRAVITY_HINT_NATURAL;
                _is_vertical = true;
            } else if (value == "upright") {
                _base_gravity = Pango::GRAVITY_EAST;
                _gravity_hint = Pango::GRAVITY_HINT_STRONG;
                _is_vertical = true;
            } else if (value == "sideways") {
                _base_gravity = Pango::GRAVITY_SOUTH;
                _gravity_hint = Pango::GRAVITY_HINT_STRONG;
                _is_vertical = true;
            } else {
                std::cerr << "SPStyle::SPStyle: invalid value for 'text-orientation':" << value << std::endl;
            }
        } else if (property == "letter-spacing") {
            _letter_spacing = std::stod(value);
        } else if (property == "word-spacing") {
            _word_spacing = std::stod(value);
        } else if (property == "fill" || property == "color") {
            _fill = Paint(value);
        } else if (property == "stroke") {
            _stroke = Paint(value);
        } else {
            std::cerr << "SPStyle::SPStyle: unhandled property: " << property << std::endl;
        }
    }

    if (_letter_spacing != 0.0) {
        if (!_font_features.empty()) {
            _font_features += ", ";
        }
        _font_features += "liga 0, clig 0";
    }
}

Glib::ustring
SPStyle::write() {
    Glib::ustring style;

    style += "font-family:" + _font_family + ";";
    style += "font-size:"   + std::to_string(_font_size)   + ";";
    style += "fill:"        + get_fill().write() + ";";
    return style;
}

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
