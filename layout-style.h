
#ifndef INKSCAPE_STYLE_H
#define INKSCAPE_STYLE_H

#include <iostream>
#include <vector>

#include <glibmm/regex.h>

#include <pangomm.h>

// ------ DUMMY SVG STYLING ------
class Paint {
public:
    Paint() {}
    Paint(Glib::ustring paint);

    double get_r() { return r; }
    double get_g() { return g; }
    double get_b() { return b; }

    guint16 get_r16() { return r * 65535; }
    guint16 get_g16() { return g * 65535; }
    guint16 get_b16() { return b * 65535; }

    Glib::ustring write();

private:
    double r = 0.0;
    double g = 0.0;
    double b = 0.0;
};

class SPStyle {
public:
    SPStyle() {}
    SPStyle(Glib::ustring style) { parse (style); }

    void parse(Glib::ustring style);

    Glib::ustring  get_font_family()    { return _font_family; }
    double         get_font_size()      { return _font_size; }
    Glib::ustring  get_font_features()  { return _font_features; }

    Pango::Gravity get_base_gravity()   { return _base_gravity; }
    Pango::GravityHint get_gravity_hint() { return _gravity_hint; }
    Pango::Direction   get_direction()  { return _direction; }

    double         get_letter_spacing() { return _letter_spacing; }
    double         get_word_spacing()   { return _word_spacing; }

    Paint&         get_fill()           { return _fill; }
    Paint&         get_stroke()         { return _stroke; }

    void set_base_gravity(Pango::Gravity base_gravity) { _base_gravity = base_gravity; }
    void set_gravity_hint(Pango::GravityHint gravity_hint) { _gravity_hint = gravity_hint;  }
    void set_direction(Pango::Direction direction) { _direction = direction; }

    bool is_vertical() { return _is_vertical; }

    Glib::ustring write();

private:

    Glib::ustring _font_family = "Dejavu Sans";
    double _font_size = 12.0;
    Glib::ustring _font_features;

    Glib::ustring _text_orientation;
    Pango::Gravity     _base_gravity = Pango::GRAVITY_AUTO;
    Pango::GravityHint _gravity_hint = Pango::GRAVITY_HINT_NATURAL;
    Pango::Direction _direction = Pango::DIRECTION_LTR;

    double _letter_spacing = 0.0;
    double _word_spacing = 0.0;

    Paint _fill;
    Paint _stroke;

    bool _is_vertical = false;
};


#endif // INKSCAPE_STYLE_H

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
