
// Comparison between using Pango font, FreeType font, and OpenType font to create HarfBuzz font.

// Compile:
// g++ -g -o hb_font_test `pkg-config --cflags --libs pangocairo pangoft2 freetype2` -lm hb_font_test.c

// Some code from:
// https://github.com/harfbuzz/harfbuzz-tutorial/blob/master/hello-harfbuzz-freetype.c

#include <stdbool.h>

#include <pango/pangoft2.h>   // pango_ft2_font_map_new()
#include <pango/pangocairo.h>

#include <ft2build.h>
#include FT_TRUETYPE_TABLES_H

#include <harfbuzz/hb-ft.h>
#include <harfbuzz/hb-ot.h>

// Common shaping routines using HarfBuzz.
hb_buffer_t* my_shape (hb_font_t* hb_font, bool vertical, const gchar* text, int offset, int length, double scale)
{
  /* Create hb-buffer and populate. */
  hb_buffer_t *hb_buffer = hb_buffer_create ();
  hb_buffer_add_utf8 (hb_buffer, text, -1, offset, length);
  if (vertical) {
    hb_buffer_set_direction (hb_buffer, HB_DIRECTION_TTB);
  }
  hb_buffer_guess_segment_properties (hb_buffer);

  /* Shape it! */
  hb_shape (hb_font, hb_buffer, NULL, 0);

  /* Get glyph information and positions out of the buffer. */
  unsigned int len = hb_buffer_get_length (hb_buffer);
  hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
  hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, NULL);

  /* Print them out as is. */
  for (unsigned int i = 0; i < len; i++) {
    hb_codepoint_t gid   = info[i].codepoint;
    unsigned int cluster = info[i].cluster;
    double x_advance = pos[i].x_advance / scale;
    double y_advance = pos[i].y_advance / scale;
    double x_offset  = pos[i].x_offset  / scale;
    double y_offset  = pos[i].y_offset  / scale;
    char glyphname[32];
    hb_font_get_glyph_name (hb_font, gid, glyphname, sizeof (glyphname));

    hb_position_t x_origin = 0.0;
    hb_position_t y_origin = 0.0;
    hb_font_get_glyph_v_origin (hb_font, gid, &x_origin, &y_origin);

    printf ("glyph='%12s'  cluster=%3d  advance=(%6.1f,%6.1f)  offset=(%6.1f,%6.1f)  origin=(%6.1f,%6.1f)",
            glyphname, cluster, x_advance, y_advance, x_offset, y_offset,
            x_origin / scale,
            y_origin / scale);

    hb_glyph_extents_t glyph_extents;
    if (hb_font_get_glyph_extents (hb_font, gid, &glyph_extents)) {
      printf ("  x_bearing=(%6.1f)  y_bearing=(%6.1f)  width=(%6.1f)  height=(%6.1f)\n",
              glyph_extents.x_bearing / scale,
              glyph_extents.y_bearing / scale,
              glyph_extents.width     / scale,
              glyph_extents.height    / scale);
    } else {
      printf ("\t No extents info!\n");
    }
  }

  return hb_buffer;
}

// Common parts for using Pango fonts with different maps.
void my_pango_shape (PangoFontMap* font_map, const char* fontname, const char* fontsize, const char* text, bool vertical)
{
  PangoContext* context = pango_font_map_create_context (font_map);

  if (vertical) {
    pango_context_set_base_gravity (context, PANGO_GRAVITY_EAST);
    pango_context_set_gravity_hint (context, PANGO_GRAVITY_HINT_STRONG);
  }

  char description_text[100] = "";
  strncat(description_text, fontname, 95);
  strncat(description_text, fontsize, 4);
  printf ("Pango font description: |%s| |%s| %s\n", fontname, fontsize, description_text);

  PangoFontDescription* description = pango_font_description_from_string(description_text);
  PangoAttrList* attrs = pango_attr_list_new();
  PangoAttribute* attr_descr = pango_attr_font_desc_new (description);
  pango_attr_list_insert (attrs, attr_descr);

  // Doesn't actually work (need to set directly on context)!
  if (vertical) {
    PangoAttribute* attr_gravity = pango_attr_gravity_new(PANGO_GRAVITY_EAST);
    pango_attr_list_insert (attrs, attr_gravity);
  }

  PangoAttrIterator* cached_iter = pango_attr_list_get_iterator(attrs);

  GList* items = pango_itemize (context, text, 0, strlen(text), attrs, cached_iter);
  for (GList* current_item = items; current_item != NULL; current_item = current_item->next) {
    PangoItem* item = (PangoItem*)current_item->data;
    PangoFont* pango_font = item->analysis.font;
    printf ("Pango font: %s\n", pango_font_description_to_string( pango_font_describe (pango_font) ) );
    hb_font_t* hb_font_pango = pango_font_get_hb_font(pango_font);
    hb_buffer_t* hb_buffer = my_shape (hb_font_pango, vertical, text, item->offset, item->length, PANGO_SCALE);
    hb_buffer_destroy (hb_buffer);
  }
}

int main (int argc, char **argv)
{
  if (argc < 3) {
    printf ("usage: hb_font_test font-file.ttf font-name text [vertical]\n");
    printf (" Example:\n");
    printf ("  ./hb_font_test /usr/share/fonts/geomtest/GeomTest-Regular.ttf GeomTest \"AADDEEFF\" vertical\n");
    exit (1);
  }

  const char* fontfile = argv[1];
  const char* fontname = argv[2];
  const char* text = argv[3];
  bool vertical = argc > 4;

  const int FONT_SIZE = 100;

  printf ("\nShaping: %s  %s  %s\n", text, fontname, vertical ? "vertical" : "horizontal");

  // *************  Pango to HarfBuzz font  ****************

  // Cairo font map

  printf ("\nPango to HarfBuzz: Cairo font map:\n");

  PangoFontMap*   font_map_cairo = pango_cairo_font_map_new(); // Set size in points.
  my_pango_shape (font_map_cairo, fontname, "  75", text, vertical);

  // FreeType font map

  printf ("\nPango to HarfBuzz: FreeType font map:\n");

  PangoFontMap*   font_map_ft = pango_ft2_font_map_new(); // Set size in pixels.
  my_pango_shape (font_map_ft, fontname, " 100", text, vertical);

  // ************* FreeType to HarfBuzz font ***************

  FT_Library ft_library;
  FT_Face ft_face;
  FT_Error ft_error;

  if ((ft_error = FT_Init_FreeType (&ft_library)))
    abort();
  if ((ft_error = FT_New_Face (ft_library, fontfile, 0, &ft_face)))
    abort();
  if ((ft_error = FT_Set_Char_Size (ft_face, FONT_SIZE*PANGO_SCALE, FONT_SIZE*PANGO_SCALE, 0, 0)))
    abort();

  /* Create hb-ft font. */
  hb_font_t* hb_font_ft = hb_ft_font_create (ft_face, NULL);
  if (vertical) {
    hb_ft_font_set_load_flags (hb_font_ft, FT_LOAD_VERTICAL_LAYOUT); // Not setting this flag causes very small differences in glyph extents.
  }

  printf ("\nFreeType to HarfBuzz:\n");
  my_shape (hb_font_ft, vertical, text, 0, -1, PANGO_SCALE);


  // ************* OpenType/HarfBuzz to HarfBuzz font ***************

  /* Create a font. */
  hb_blob_t* blob = hb_blob_create_from_file(fontfile);
  hb_face_t* hb_face = hb_face_create(blob, 0);
  hb_blob_destroy(blob); /* face will keep a reference to it */
  hb_font_t* hb_font = hb_font_create(hb_face);

  hb_ot_font_set_funcs(hb_font);
  hb_font_set_scale(hb_font, FONT_SIZE*PANGO_SCALE, FONT_SIZE*PANGO_SCALE);

  hb_buffer_t* hb_buffer = NULL;

  printf ("\nHarfBuzz/OpenType to HarfBuzz:\n");
  hb_buffer = my_shape (hb_font, vertical, text, 0, -1, PANGO_SCALE);
  hb_buffer_destroy (hb_buffer);

  /* Now try it with FreeType */
  hb_ft_font_set_funcs(hb_font);

  printf ("\nHarfBuzz/FreeType to HarfBuzz:\n");
  hb_buffer = my_shape (hb_font, vertical, text, 0, -1, PANGO_SCALE);
  hb_buffer_destroy (hb_buffer);

  // Clean up
  hb_font_destroy (hb_font);
  hb_face_destroy (hb_face);

  FT_Done_Face (ft_face);
  FT_Done_FreeType (ft_library);

}
