# Test Fonts

Specially created fonts for testing font metrics.

- Fonts:
    - GeomTest-Regular.ttf: with vertical metrics.
    - GeomTestNoVertical-Regular.ttf: without vertical metrics.

- The design space:
    - 1000x1000 with the alphabetic baseline 200 units above the bottom of the em-box and the hanging baseline 200 units below the top of the em-box.

- Glyphs:
    - 'A' Em-box size glyph with 'A'. Anchor centered at top of em-box.
    - 'B' Em-box size glyph without letter. Anchor centered at top of em-box.
    - 'C' Em-box size glyph with triangle. No anchor.
    - 'D' Half-width glyph with triangle. Anchor centered at top of glyph.
    - 'E' Half-width glyph with triangle, extending from -200 units below to 400 units above the bottom of the em-box. Anchor centered at top of glyph.
    - 'F' Same as 'E' but quarter-width.
    - 'G' Double width glyph. Anchor centered at top of glyph.
    - 'H' Small hexagon centered on alphabetic baseline with advance of 235 units.
    - 'x' An 'x' with x-height of 600 units (above alphabetic baseline).
    - 'y' A half-width glyph extending from alphabetic baseline to top of em-box.
    - 'z' Same as 'y' but with a different shape inside.
    - '|' A zero-width glyph with some numbers marking distances from alphabetic baseline.
    - 'uni030A' A non-spacing mark with em-box width. Right edge of glyph at zero. Anchor centered at top of glyph.
    - 'uni030B' Same as uni030A except half-width with anchor at top-left corner.
    - 'uni030C' A non-spacing mark with em-box width. Glyph is centered horizontally at zero and vertically 400 units below em-box top. Anchor is centered at top of glyph.
    - 'uni0904' Embox size glyph with 'H' (for hanging). Top of 'H' is at hanging baseline.
    - 'uni092E' A 'म'. Useful for measuring position of hanging baseline.
    - '−' (minus) Useful for measuring position of mathematical baseline.
    - 'uni30FC' (em-dash) Em-box size glyph with "dash" centered in em-box. See next glyph.
    - 'uni30FC.vert' (em-dash) Em-box size glyph with "dash" centered in em-box and 'v' below dash. An alternative glyph for uni30FC for vertical text layout.
    - '囗' An example ideographic glyph.
    - '囘' Embox size glyph with 'I' (for ideographic).
    - '囙' A double-height glyph for checking vertical advance. Glyph is centered on em-box bottom with a hexagon in bottom half.
        
