
#ifndef INKSCAPE_TEXT_LAYOUT_H
#define INKSCAPE_TEXT_LAYOUT_H

#include <iostream>

#include <cairomm/cairomm.h>
#include <pangomm.h>
#include <pangomm/glyphstring.h>
#include <boost/optional.hpp>

#include "layout-style.h"

/*
It's imperitive that one keeps track of where characters are used vs. where bytes are used.
(A UTF-8 character often consists of more than one byte.)
*/

// ------ DUMMY SVG LENGTH ------
class SVGLength {
public:
    SVGLength(double value) : _value(value) {}
    double _value;
};

class FontInstance;

class Layout {

public:
    Layout(PangoFontMap* server);
    ~Layout() = default;

    class TextItem;
    class Text;

    // Public members

    void set_base_direction (Pango::Direction   base_direction);
    void set_base_gravity   (Pango::Gravity     base_gravity);
    void set_gravity_hint   (Pango::GravityHint gravity_hint);

    // Create new text holding object.
    void new_text();
    // Store text into spans.
    // Note: positioning attributes must already be cascaded properly.
    void add_text(const Glib::ustring& text, SPStyle* style,
                  std::vector<SVGLength> x = std::vector<SVGLength>(),
                  std::vector<SVGLength> y = std::vector<SVGLength>(),
                  std::vector<SVGLength> dx = std::vector<SVGLength>(),
                  std::vector<SVGLength> dy = std::vector<SVGLength>(),
                  std::vector<SVGLength> rotate = std::vector<SVGLength>());
    Glib::ustring get_text() { return _text; }

    void itemize();

    void render(Cairo::RefPtr<Cairo::Context>& context);

    // Debug utilities
    void dump_texts();
    void dump_spans();
    void dump_item(const Pango::Item& item);
    void dump_hb_buffer(hb_font_t* hb_font, hb_buffer_t* hb_buffer);
    void dump_buffers(const Glib::RefPtr<Pango::Font> pango_font,
                      const Pango::GlyphString& glyph_string,
                      hb_buffer_t* hb_buffer);

    // ====== Input ======
    // Set of characters with same style properties.
    class Span {
    public:

        Span(Layout& l, const Glib::ustring& text, SPStyle* style,
             boost::optional<SVGLength> x      = boost::optional<SVGLength>(),
             boost::optional<SVGLength> y      = boost::optional<SVGLength>(),
             boost::optional<SVGLength> dx     = boost::optional<SVGLength>(),
             boost::optional<SVGLength> dy     = boost::optional<SVGLength>(),
             boost::optional<SVGLength> rotate = boost::optional<SVGLength>());

        Span(Text& t, const Glib::ustring& text, SPStyle* style,
             boost::optional<SVGLength> x      = boost::optional<SVGLength>(),
             boost::optional<SVGLength> y      = boost::optional<SVGLength>(),
             boost::optional<SVGLength> dx     = boost::optional<SVGLength>(),
             boost::optional<SVGLength> dy     = boost::optional<SVGLength>(),
             boost::optional<SVGLength> rotate = boost::optional<SVGLength>());

        ~Span() = default;

        int _start_index_char = -1; // Index into _text (and _char);
        int _start_index_byte = -1; // Index into _text;
        int _length_char = 0;  // Number of characters in span (could be calculated but easier to just store).
        int _length_byte = 0;

        SPStyle* get_style() { return &_style; }

    private:
        // --- Inputs ---
        SPStyle _style;

        // SVG positioning attributes. Maximum one per span.
        boost::optional<SVGLength> _x;
        boost::optional<SVGLength> _y;
        boost::optional<SVGLength> _dx;
        boost::optional<SVGLength> _dy;
        boost::optional<SVGLength> _rotate;

    public:
        std::vector<TextItem *> _text_items; // Our item
        std::vector<Pango::Item> _pango_items; // Owned by span.
        Pango::FontDescription _font_description;
    };

    // ====== Shaping ======

    class Glyph {
    public:
        Glyph(hb_glyph_info_t* hb_glyph_info, hb_glyph_position_t* hb_glyph_position);

        hb_glyph_info_t* get_hb_glyph_info() { return _hb_glyph_info; }
        hb_glyph_position_t* get_hb_glyph_position() { return _hb_glyph_position; }

    private:
        hb_glyph_info_t*     _hb_glyph_info     = nullptr; // _codepoint, cluster, glyph_flags.
        hb_glyph_position_t* _hb_glyph_position = nullptr; // Probably not needed (use _h_advance, etc.).
    };

    class Cluster {
    public:
        Cluster(Layout::TextItem *text_item)
            : _text_item(text_item)
        {}

        int get_index_span() { return _index_span; }
        int get_index_item() { return _index_item; }

        void add_index_character(int index_character) { _index_characters.push_back(index_character); }
        std::vector<int>& get_index_characters() { return _index_characters; }

        void add_index_glyph(int index_glyph) { _index_glyphs.push_back(index_glyph); }
        std::vector<int>& get_index_glyphs() { return _index_glyphs; }

        void add_glyph(Glyph glyph) { _glyphs.emplace_back(glyph); }
        std::vector<Glyph>& get_glyphs() { return _glyphs; }

        // Geometry
        void set_x_advance(double x_advance) { _x_advance = x_advance; }
        void set_y_advance(double y_advance) { _y_advance = y_advance; }
        double get_x_advance() { return _x_advance; }
        double get_y_advance() { return _y_advance; }

        // Properties
        void set_log_attributes(PangoLogAttr log_attributes) { _log_attributes = log_attributes; }
        bool is_white()      { return _log_attributes.is_white;           }
        bool is_word_start() { return _log_attributes.is_word_start;      }
        bool is_word_end()   { return _log_attributes.is_word_end;        }

    private:
        int _index_span = -1;
        int _index_item = -1;

        Layout::TextItem * _text_item = nullptr;
        std::vector<int> _index_characters; // Index into Pango item subtext.
        std::vector<int> _index_glyphs;  // Index into harfbuzz buffer.
        std::vector<Glyph> _glyphs;

        double _x_advance = 0.0;
        double _y_advance = 0.0;

        PangoLogAttr _log_attributes; // Break, white space, etc. (for first character in cluster).
    };


    class TextItem {
    public:
        TextItem (Pango::Item* pango_item, Span* span, int index_char, Layout* layout );
        ~TextItem () { std::cout << "TextItem Destructor!" << std::endl; }

        Pango::Item* get_pango_item() { return _pango_item; }

        void set_x_advance(double x_advance) { _x_advance = x_advance; }
        void set_y_advance(double y_advance) { _y_advance = y_advance; }
        double get_x_advance() { return _x_advance; }
        double get_y_advance() { return _y_advance; }

        SPStyle* get_style() { return _span->get_style(); }

        int get_letter_count() { return _letter_count; }
        int get_word_count()   { return _word_count;   }

        // private:
        Layout* _layout; // TODO: Remove need!
        Span* _span;
        int _index_span = -1;
        int _index_char = -1; // Index into layout text string and character logical attributes vector.

        // FontInstance* _font_instance = nullptr;

        Pango::Item* _pango_item;
        Pango::GlyphString _pango_glyph_string; // TEMP TEMP TEMP
        hb_buffer_t* _hb_buffer = nullptr;

        std::vector<Cluster> _clusters;

        double _x_advance = 0.0;
        double _y_advance = 0.0;

        int _letter_count = 0; // Used for 'letter-spacing'.
        int _word_count   = 0; // Used for 'word-spacing'.
    };

    // Input
    PangoFontMap* _server = nullptr;

    // Pango Context
    Glib::RefPtr<Pango::Context> _pango_context;
    Pango::Direction   _base_direction = Pango::DIRECTION_LTR;
    Pango::Gravity     _base_gravity   = Pango::GRAVITY_AUTO;
    Pango::GravityHint _gravity_hint   = Pango::GRAVITY_HINT_NATURAL;

    Glib::ustring _text;        // All characters in text
    Pango::AttrList _attrs;     // Pango Attributes, index via _text
    std::vector<PangoLogAttr> _char_attributes;

    std::vector<Span *> _spans;  // Input spans (text within one SVG element, may be more than one per element if element contains child tspans).
    std::vector<TextItem *> _text_items;
};

#endif // INKSCAPE_TEXT_LAYOUT_H

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
