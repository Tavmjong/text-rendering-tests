# Single Line Rendering Tests

See layout-single-line-main.cpp, layout-single-line.cpp, etc.

The test simulates laying out a single line of SVG text with variations:

- horizontal vs. vertical layout (mixed, upright, and sideways),
- right-to-left text embedded in left-to-right text (and vice versa),
- mixed languages,
- Pango versions: pre 1.48.1, 1.48.1 and 1.48.2, post 1.48.2 (with special Pango build).

Six different rendering paths utilizing FreeType, HarfBuzz, Pango, and Cairo were tried:

 *   Pango GlyphString to Cairo glyph.
 *   HarfBuzz hb_buffer to Cairo glyph.
 *   Pango GlyphString to FreeType Paths.
 *   HarfBuzz shaping, clusters, to cairo\_show\_glyph\_string().
 *   Pango GlyphString to pango\_cairo\_show\_glyph\_string().
 *   Pango Layout directly.

Note: Fonts used must be installed on system.

## Conclusions

The optimal solution is:

- Use Pango to "itemize" text. (Itemization break the text into spans that use the same font which can be language dependent.)
- Use HarfBuzz to "shape" text. (Determine which glyphs and their positions to render the text.)
- Layout the text using our own routines.
- Use Cairo for rendering (probably via path extracted from FreeType).

### Discussion:

#### Itemization

Pango provides the only solution for breaking text into spans for rendering and choosing the optimal font for each span.

#### Shaping

HarfBuzz is optimal for shaping due to inadequacies in Pango with vertical upright glyph positioning with fonts that lack vertical metrics (the vast majority of fonts).

HarfBuzz has two set of functions which influence how glyphs are positioned: "OpenType" and "FreeType". Pango uses HarfBuzz shaping under-the-hood with the "OpenType" functions. Harfbuzz defaults to the "OpenType" functions. Both sets of functions do not optimally position vertical upright glyphs from fonts without vertical matrics, either positioning them so that the top of a glyph's "ink-rectangle" is at the top of the "em box" grid ("OpenType" functions) or so that the glyphs are centered vertically in the "em box" grid ("FreeType" functions). The desired positioning is to match that of fonts with vertical metrics, aligning the "em-box" of the font to the "em box" grid. See figure.

<p align="center">
![Vertical Upright Glyph Placement](figures/vertical_upright_glyph_placement.png)

We can adjust the vertical alignment by shifting the glyphs down by the glyph ascent (distance between the top of the glyph's ink-rectangle and the alphabetic baseline). To do this, we need to know the position of the alphabetic baseline which is not readily available from Pango (which reports the vertical baseline). An additional complication is that all glyphs in a cluster must be shifted by the same amount. Pango marks the first character in a cluster (i.e., the 'f' in 'fi') but does not mark the first glyph in a cluster (i.e., the 'x' in 'x̂' when 'x̂' is composed of two glyphs). HarfBuzz does allow one to do the re-positioning properly.

Note: Pango 1.48.1 introduces a change in positioning of vertical-upright glyphs. A minus sign error is corrected and glyph positioning is corrected by the difference between the vertical and horizontal glyph alignment points. The alignment point correction is needed as Pango generates a Cairo font with the "vertical layout" flag set. This causes Cairo to shift the glyphs by the difference between the vertical and horizontal alignment points. HarfBuzz, however, already includes this shift in the positions it produces. Pango 1.48.1 attempts to undo this "double shift". However the shift of glyphs by Cairo is equivalent to the shift of glyphs as calculated by HarfBuzz using the "FreeType" functions while the correction in Pango is calculated via HarfBuzz using the "OpenType" functions. This leads to incorrect glyph positioning, especially noticeable with anchored glyphs (accents, etc.) which get a different correction than their "parent" glyph.

#### Layout

The only reasonable option is for us to layout the text ourselves in order to:

- Correct glyph placement in vertical-upright text.
- Handle embedding right-to-left text inside left-to-right text.
- Correct glyph rendering order in 'right-to-left' text where SVG dictates the glyphs should be drawn in the order that the characters occur in the text (Pango/HarfBuzz order the glyphs visually from left-to-right).
- Properly align vertical-sideways text.
- Do line-breaking inside arbitrary shapes (Pango::Layout is limited to fixed width regions).
- Apply SVG positioning attributes and properties.

#### Rendering

We rely on Cairo for rendering in Inkscape. Current Inkscape uses FreeType to extract out the path data for glyphs which is the rendered by Cairo as paths. This seems to work well (and we need to be able to extract out path data for our Text->Path function). An alternative would be to use CairoContext::show\_glyphs() after filling an array of Cairo::Glyphs with the glyph id and positions. Using the GlyphString found by Pango is a no-go due to glyph-positioning problems. Using Pango::Layout is a double-no-go as it doesn't handle mixed direction text,  as well as having glyph-positioning problems.
