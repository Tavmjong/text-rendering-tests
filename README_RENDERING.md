# Rendering Tests

See hb\_render\_test.c

Four runs:

- Using font with vs. without vertical metrics.
- Horizontal vs. vertical layout.

Each run produced a PNG file with eight renderings (numbers refer to row or column):

- Functions:
    - 1-4 HarfBuzz OpenType functions.
    - 5-8 HarfBuzz FreeType functions.

- Vertical metrics:
    - 1, 2, 5, 6: Do not load with vertical metrics.
    - 3, 4, 7, 8: Load with vertical metrics.

- Source of glyph paths:
    - 1, 3, 5, 7: Use glyph paths directly from FreeType
    - 2, 4, 6, 8: Use glyph paths from Cairo (cairo\_show\_glyphs()) after creating Cairo font via cairo\_ft\_font\_face\_create\_for\_ft\_face().
    
## Conclusions

- Glyphs positioned using Cairo font loaded with the FreeType flag *FT\_LOAD\_VERTICAL\_LAYOUT* are always incorrectly positioned.
- FreeType *FT\_LOAD\_VERTICAL\_LAYOUT* flag has no effect on the path loaded via FT\_Outline\_Decompose() (when applied to FT\_Load\_Glyph()).

In particular:

- Layouts 1, 2, 3, 5, 6, 7 always agree:
    - Horizontally (independent of using/not-using vertical metrics).
    - Vertically (except for differences between fonts with/without vertical metrics, see below).

- Layouts 4, 8:
    - Glyphs are shifted relative to the other layouts:
        - *x*: left by half of glyph width
        - *y*: down by ascent when using vertical metrics, down by ascent after centering vertically the glyphs when not using vertical metrics. Anchored glyphs are **not** placed correctly relative to parent glyph.

- Vertical layout glyph positioning (except 4, 8):
     - With vertical metrics in font: Glyphs are positioned vertically as they are positioned within em-box for horizontal text.
     - Without vertical metrics in font:
         - OpenType functions: Glyphs are aligned so that the top of their ink-rectangle is aligned to the top of the em-box.
         - FreeType functions: Glyphs are visually centered inside em-box.
     - In all cases, anchored glyphs are placed relative to their parent glyph correctly.