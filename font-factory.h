
#ifndef INKSCAPE_FONT_FACTORY_H
#define INKSCAPE_FONT_FACTORY_H

#include <unordered_map>

#include <pangomm.h>

class FontInstance;

// Singleton. Caches font instances.
class FontFactory {
public:
    static FontFactory& get_instance() {
        static FontFactory instance;
        return instance;
    }

    FontInstance* get_font_instance(Pango::FontDescription description);
    void dump_font_instance_map();

private:
    FontFactory();

public:
    FontFactory(FontFactory const&)    = delete;
    void operator=(FontFactory const&) = delete;

private:
    Glib::RefPtr<Pango::FontMap> _pango_font_map;
    Glib::RefPtr<Pango::Context> _pango_context;

    std::unordered_map<unsigned int, FontInstance*> _font_instance_map;
};

#endif // INKSCAPE_FONT_FACTORY_H

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
