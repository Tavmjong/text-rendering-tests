# Text Rendering Tests

Tests of the FreeType/Pango/Harfbuzz/Cairo rendering chain.

- [HarfBuzz font creation and shaping](README_FONT_CREATION.md)
    - hb\_font\_test.c
    - hb\_font\_test.scr
- [Rendering tests](README_RENDERING.md) \(Does not use Pango.\)
    - hb\_rendering\_test.c
    - hb\_rendering\_test.scr
- [Single line layout test](README_SINGLE_LINE.md)
    - layout-single-line-main.cpp
    - layout-single-line.cpp/.h
    - font-instance.cpp/.h
    - font-factory.cpp/.h
    - layout-single-line.scr
- [Test fonts](README_FONTS.md) (See fonts directory.)
