

#include "font-factory.h"

#include <iostream>
#include <iomanip>

#include <pango/pangoft2.h>
#include <pango/pangocairo.h>

#include "font-instance.h"

FontFactory::FontFactory()
{
    _pango_font_map = Glib::wrap(pango_ft2_font_map_new());
    // _pango_font_map = Glib::wrap(pango_cairo_font_map_get_default ());

    _pango_context = _pango_font_map->create_context();
}

/**
 * Create a font instance with a standard size.
 */
FontInstance*
FontFactory::get_font_instance(Pango::FontDescription description)
{
    description.set_size(512*PANGO_SCALE); // Standard size.

    unsigned int hash = description.hash();
    std::cout << "\nFontFactory::get_font_instance: " << description.to_string() << "  Hash: " << hash << std::endl;

    auto it = _font_instance_map.find(hash);
    if (it != _font_instance_map.end()) {
        return it->second;
    }

    auto pango_font = _pango_font_map->load_font(_pango_context, description);
    auto font_instance = new FontInstance(pango_font);
    _font_instance_map[hash] = font_instance;

    return font_instance;
}

void
FontFactory::dump_font_instance_map()
{
    std::cout << "\nFontFactory FontInstance map:" << std::endl;
    for (auto const& pair: _font_instance_map) {
        std::cout << "  " << pair.second->get_description().to_string() << std::endl;
    }
}

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
