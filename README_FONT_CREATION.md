# HarfBuzz font creation and shaping.

See hb\_font\_test.c

Five modes of hb\_font\_t creation are tested:

1. Pango font from Cairo font map (pango\_font\_get\_hb\_font()).
2. Pango font from FreeType font map (pango\_font\_get\_hb\_font()).
3. FreeType (hb\_ft\_font\_create()).
4. HarfBuzz blob (hb\_font\_create()) with OpenType functions (hb\_ot\_font\_set\_funcs()).
5. HarfBuzz blob (hb\_font\_create()) with FreeType functions (hb\_ft\_font\_set\_funcs()).

Font creation was tested for:

- Horizontal layout.
- Vertical layout with upright glyphs with a font with vertical font metrics.
- Vertical layout with upright glyphs with a font with no vertical font metrics.

Note: **hb\_font\_test.scr*, requires the fonts GeomTest-Regular.ttf and GeomTestNoVertical-Regular.ttf to be available to Pango.

## Conclusions:

- Anchored glyphs are properly placed relative to "parent" in all cases.
- Without vertical metrics, upright glyphs are placed either with the top of their ink rectangle at the alignment point (modes 1, 2, 4) or centered within an embox with its top at the alignment point (modes 3, 5).
- y\_origin for anchored glyphs is not adjusted (unlike y\_offset) for parent's position.
- Pango font-description font-size represents points with Cairo font map but pixels with FreeType font map.

In particular  (*dA* = anchor point of parent − anchor point of glyph), y coordinate increases upward:

### Offset

hb\_buffer\_get\_glyph\_positions():

- **x\_offset**
    - Non-anchored glyphs:
        - Horizontal layout: usually 0.
        - Vertical upright layout: −half of glyph width.
    - Anchored glyphs:
        - −x\_advance of parent + x\_offset of parent + *dA<sub>x</sub>*.

- **y\_offset** (hb\_buffer\_get\_glyph\_positions()):
    - Non-anchored glyphs:
        - Horizontal layout: usually 0.
        - Vertical upright layout:
            - Fonts with vertical metrics: −font ascent (alphabetic baseline − top of em-box, negative).
            - Fonts without vertical metrics:
                - Modes 3, 5: alphabetic baseline − vertical center of ink rectangle (usually negative) − half of em-box. 
                - Modes 1, 2, 4: -glyph ascent (alphabetic baseline - top of ink rectangle).

    - Anchored glyphs:
        - −y\_advance of parent + y\_offset of parent + *dA<sub>y</sub>*.

### Origin

hb\_font\_get\_glyph\_v\_origin():

Pango uses Harfbuzz for shaping (since 1.44.0). Pango uses HarfBuzz's defaults which use the OpenType functions to calculate offset. Pango loads Cairo fonts with FC\_VERTICAL\_LAYOUT flag set which shifts glyphs by offset as calculated by FreeType functions. This leads to improper glyph placement in Pango 1.48.1 and 1.48.2 for vertical upright glyphs as Pango attempts to correct Cairo font shift with values from this function.

- **x\_origin**
    - Vertical layout:
        - −half width (−x\_offset for non-anchored glyphs, typically 0 for anchored glyphs).

- **y\_origin**
    - Vertical layout:
        - −y\_offset as calculated assuming glyph is non-anchored.
