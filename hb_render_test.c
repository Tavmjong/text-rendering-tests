
// Rendering test for HarfBuzz using FreeType/OpenType functions and for vertical/horizontal text with/without vertical metrics.

// Compile:
// g++ -g -o hb_render_test `pkg-config --cflags --libs pangocairo pangoft2 freetype2` -lm hb_render_test.c

// Some code from:
// https://github.com/harfbuzz/harfbuzz-tutorial/blob/master/hello-harfbuzz-freetype.c

#include <stdbool.h>

#include <cairo.h>
#include <cairo-ft.h>

#include <pango/pangoft2.h>   // pango_ft2_font_map_new()
#include <pango/pangocairo.h>

#include <ft2build.h>
#include FT_TRUETYPE_TABLES_H
#include FT_OUTLINE_H

#include <harfbuzz/hb-ft.h>
#include <harfbuzz/hb-ot.h>

void get_cairo_path_for_glyph (cairo_t* context, FT_Face ft_face, unsigned int glyph_id, bool load_vertical);

// Common shaping routines using HarfBuzz.
hb_buffer_t* my_shape (hb_font_t* hb_font, bool vertical, const gchar* text, int offset, int length, double scale)
{
  /* Create hb-buffer and populate. */
  hb_buffer_t *hb_buffer = hb_buffer_create ();
  hb_buffer_add_utf8 (hb_buffer, text, -1, offset, length);
  if (vertical) {
    hb_buffer_set_direction (hb_buffer, HB_DIRECTION_TTB);
  }
  hb_buffer_guess_segment_properties (hb_buffer);

  /* Shape it! */
  hb_shape (hb_font, hb_buffer, NULL, 0);

  /* Get glyph information and positions out of the buffer. */
  unsigned int len = hb_buffer_get_length (hb_buffer);
  hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
  hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, NULL);

  /* Print them out as is. */
  for (unsigned int i = 0; i < len; i++) {
    hb_codepoint_t gid   = info[i].codepoint;
    unsigned int cluster = info[i].cluster;
    double x_advance = pos[i].x_advance / scale;
    double y_advance = pos[i].y_advance / scale;
    double x_offset  = pos[i].x_offset  / scale;
    double y_offset  = pos[i].y_offset  / scale;
    char glyphname[32];
    hb_font_get_glyph_name (hb_font, gid, glyphname, sizeof (glyphname));

    hb_position_t x_origin = 0.0;
    hb_position_t y_origin = 0.0;
    hb_font_get_glyph_v_origin (hb_font, gid, &x_origin, &y_origin);

    printf ("glyph='%12s'  cluster=%3d  advance=(%6.1f,%6.1f)  offset=(%6.1f,%6.1f)  origin=(%6.1f,%6.1f)",
            glyphname, cluster, x_advance, y_advance, x_offset, y_offset,
            x_origin / scale,
            y_origin / scale);

    hb_glyph_extents_t glyph_extents;
    if (hb_font_get_glyph_extents (hb_font, gid, &glyph_extents)) {
      printf ("  x_bearing=(%6.1f)  y_bearing=(%6.1f)  width=(%6.1f)  height=(%6.1f)\n",
              glyph_extents.x_bearing / scale,
              glyph_extents.y_bearing / scale,
              glyph_extents.width     / scale,
              glyph_extents.height    / scale);
    } else {
      printf ("\t No extents info!\n");
    }
  }

  return hb_buffer;
}


// Draw some lines or boxes to help check glyph placement.
void draw_guides (cairo_t* context, double font_size, bool layout_vertical, double ascender)
{
  cairo_save (context);

  // Draw square at alignment point.
  cairo_rectangle (context, -4, -4, 8, 8);
  cairo_set_source_rgb (context, 1.0, 0.5, 0.5);
  cairo_fill (context);

  // Draw lines in light blue.
  cairo_set_source_rgb (context, 0.5, 0.5, 1.0);

  if (layout_vertical) {

    // Draw em-boxes.
    for (unsigned int i = 0; i < 24; i++) {
      cairo_rectangle (context, -font_size/2.0, i * font_size, font_size, font_size);
      cairo_stroke (context);
    }

  } else {

    // Baseline
    cairo_move_to (context, 0, 0);
    cairo_line_to (context, 23 * font_size, 0);
    cairo_stroke (context);

    static const double dashed[] = {5.0, 5.0};
    cairo_set_dash (context, dashed, 2, 1.0);

    // Top of embox
    cairo_move_to (context, 0, -ascender);
    cairo_line_to (context, 23 * font_size, -ascender);
    cairo_stroke (context);

    // Bottom of embox
    cairo_move_to (context, 0, font_size - ascender);
    cairo_line_to (context, 23 * font_size, font_size - ascender);
    cairo_stroke (context);
  }

  cairo_restore (context);
}

// Common routine to render glyphs via Cairo.
void my_render (cairo_t* context, hb_buffer_t* hb_buffer, FT_Face ft_face, double font_size, bool layout_vertical, bool load_vertical)
{
  cairo_set_source_rgb (context, 0.0, 0.0, 0.0);
  unsigned int len = hb_buffer_get_length (hb_buffer);
  hb_glyph_info_t *info = hb_buffer_get_glyph_infos (hb_buffer, NULL);
  hb_glyph_position_t *pos = hb_buffer_get_glyph_positions (hb_buffer, &len);

  double ascender =  ft_face->ascender / (double) ft_face->units_per_EM * font_size;

  cairo_save (context);

  // Extract glyph path from FreeType font directly.
  for (unsigned int i = 0; i < len; i++) {

    // Draw glyph.
    cairo_save (context);
    cairo_translate (context, pos[i].x_offset / (double)PANGO_SCALE, -pos[i].y_offset / (double)PANGO_SCALE);
    cairo_scale (context, font_size, -font_size); // FreeType path is scaled to unit size.
    get_cairo_path_for_glyph (context, ft_face, info[i].codepoint, load_vertical);
    cairo_fill (context);
    cairo_restore (context);

    // Move onto next glyph.
    cairo_translate (context, pos[i].x_advance / (double)PANGO_SCALE, -pos[i].y_advance / (double)PANGO_SCALE);
  }

  cairo_restore (context);

  // Draw guides on top.
  draw_guides (context, font_size, layout_vertical, ascender);

  // Move onto next layout.
  cairo_translate (context, !layout_vertical ? 0 : 2 * font_size, layout_vertical ? 0 : 2 * font_size);

  cairo_save (context);

  // Use only Cairo routines for drawing glyphs from FreeType font.
  int ft_load_flags = 0; //  = FT_LOAD_NO_SCALE; // Implies FT_LOAD_NO_HINTING and FT_LOAD_NO_BITMAP
  if (load_vertical) {
    ft_load_flags |= FT_LOAD_VERTICAL_LAYOUT;
  }
  cairo_font_face_t* cairo_font_face = cairo_ft_font_face_create_for_ft_face (ft_face, ft_load_flags);
  cairo_set_font_face (context, cairo_font_face);
  cairo_set_font_size (context, font_size);

  cairo_glyph_t* cairo_glyphs = cairo_glyph_allocate (len);
  double current_x = 0;
  double current_y = 0;
  for (unsigned int i = 0; i < len; i++) {
    cairo_glyphs[i].index = info[i].codepoint;
    cairo_glyphs[i].x =   current_x + (pos[i].x_offset / (double)PANGO_SCALE);
    cairo_glyphs[i].y = -(current_y + (pos[i].y_offset / (double)PANGO_SCALE));
    current_x += pos[i].x_advance / (double)PANGO_SCALE;
    current_y += pos[i].y_advance / (double)PANGO_SCALE;
  }
  cairo_show_glyphs (context, cairo_glyphs, len);
  cairo_glyph_free (cairo_glyphs);

  cairo_font_face_destroy (cairo_font_face);

  cairo_restore (context);

  draw_guides (context, font_size, layout_vertical, ascender);
  cairo_translate (context, !layout_vertical ? 0 : 2 * font_size, layout_vertical ? 0 : 2 * font_size);
}

int main (int argc, char **argv)
{
  if (argc < 3) {
    printf ("usage: hb_render_test font-file.ttf font-name text [vertical]\n");
    printf (" Example:\n");
    printf ("  ./hb_render_test /usr/share/fonts/geomtest/GeomTest-Regular.ttf GeomTest \"囘囙囘 AAऄऄ ÅÅA̋A̋ǍǍĚĚH\" vertical\n");
    exit (1);
  }

  const char* fontfile = argv[1];
  const char* fontname = argv[2];
  const char* text = argv[3];
  bool vertical = argc > 4;

  const double FONT_SIZE = 100.0;

  printf ("\nShaping: %s  %s  %s\n", text, fontname, vertical ? "vertical" : "horizontal");

  /* Create HarfBuzz font. */
  hb_blob_t* hb_blob = hb_blob_create_from_file(fontfile);
  hb_face_t* hb_face = hb_face_create(hb_blob, 0);
  hb_font_t* hb_font = hb_font_create(hb_face);
  hb_font_set_scale(hb_font, FONT_SIZE * PANGO_SCALE, FONT_SIZE * PANGO_SCALE); // Set to large number to prevent rounding errors.

  /* Create FreeType face */
  unsigned int hb_blob_length = 0;
  const char *hb_blob_data = hb_blob_get_data (hb_blob, &hb_blob_length);
  static FT_Library ft_library = nullptr;
  if (!ft_library) {
    FT_Init_FreeType (&ft_library);
  }
  int face_index = 0;
  FT_Face ft_face = nullptr;
  FT_Error ft_error = FT_New_Memory_Face (ft_library, (const FT_Byte*)hb_blob_data, hb_blob_length, face_index, &ft_face);
  if (ft_error != 0) {
    printf ("Failed to create FT_Face\n");
  }

  /* Create Cairo Context */
  const int width  = FONT_SIZE * (vertical ? 18 : 25);
  const int height = FONT_SIZE * (vertical ? 25 : 18);
  cairo_surface_t* surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, width, height);
  cairo_t* context = cairo_create (surface);
  cairo_set_source_rgb (context, 0.9, 0.9, 0.9);
  cairo_paint (context);

  if (vertical) {
    cairo_translate (context, 2 * FONT_SIZE, 0.5 * FONT_SIZE);
  } else {
    hb_font_extents_t hb_font_extents;
    hb_font_get_h_extents (hb_font, &hb_font_extents);
    cairo_translate (context, FONT_SIZE, FONT_SIZE + hb_font_extents.ascender/ (double)PANGO_SCALE);
  }

  hb_buffer_t* hb_buffer = nullptr;

  /* Shape and Render with OpenType functions */
  printf ("\nHarfBuzz/OpenType to HarfBuzz:\n");
  hb_ot_font_set_funcs(hb_font);

  hb_buffer = my_shape (hb_font, vertical, text, 0, -1, FONT_SIZE * PANGO_SCALE);
  my_render (context, hb_buffer, ft_face, FONT_SIZE, vertical, false);
  my_render (context, hb_buffer, ft_face, FONT_SIZE, vertical, true);
  hb_buffer_destroy (hb_buffer);

  /* Shape and Render with FreeType functions */
  printf ("\nHarfBuzz/FreeType to HarfBuzz:\n");
  hb_ft_font_set_funcs(hb_font);

  hb_buffer = my_shape (hb_font, vertical, text, 0, -1, FONT_SIZE * PANGO_SCALE);
  my_render (context, hb_buffer, ft_face, FONT_SIZE, vertical, false);
  my_render (context, hb_buffer, ft_face, FONT_SIZE, vertical, true);
  hb_buffer_destroy (hb_buffer);

  hb_blob_destroy(hb_blob);

  /* Save image */

  char filename[100];
  strncat(filename, "hb_render_test_", 20);
  if (vertical) {
    strncat (filename, "vertical_", 20);
  } else {
    strncat (filename, "horizontal_", 20);
  }
  strncat (filename, fontname, 50);
  strncat (filename, ".png", 10);
  printf ("filename: %s\n", filename);

  cairo_surface_write_to_png (surface, filename);

  // Clean up
  cairo_destroy (context);
  cairo_surface_destroy (surface);

  hb_font_destroy (hb_font);

  FT_Done_Face (ft_face);
  FT_Done_FreeType (ft_library);
}

// A hack to get glyph paths.
struct FT2GeomData {
  FT2GeomData(cairo_t* context, double scale)
    : _context(context)
    , _scale(scale)
  {}

  cairo_t* _context;
  double _scale = 1.0;
  double _last_x = 0.0;
  double _last_y = 0.0;
};


// ========= Extract path from FreeType =========

static int
ft2_move_to(FT_Vector const *to, void *user)
{
  // std::cout << "ft2_move_to: " << to->x << ", " << to->y << std::endl;
  FT2GeomData *_user = static_cast<FT2GeomData *>(user);
  double x = to->x * _user->_scale;
  double y = to->y * _user->_scale;

  cairo_move_to (_user->_context, x, y);

  _user->_last_x = x;
  _user->_last_y = y;

  return 0;
}

static int
ft2_line_to(FT_Vector const *to, void *user)
{
  // std::cout << "ft2_line_to: " << to->x << ", " << to->y << std::endl;
  FT2GeomData *_user = static_cast<FT2GeomData *>(user);
  double x = to->x * _user->_scale;
  double y = to->y * _user->_scale;

  cairo_line_to (_user->_context, x, y);

  _user->_last_x = x;
  _user->_last_y = y;

  return 0;
}

static int
ft2_conic_to(FT_Vector const *control, FT_Vector const *to, void *user)
{
  // std::cout << "ft2_conic_to: " << control->x << ", " << control->y << "  " << to->x << ", " << to->y << std::endl;
  FT2GeomData *_user = static_cast<FT2GeomData *>(user);
  double cx = control->x * _user->_scale;
  double cy = control->y * _user->_scale;
  double x = to->x * _user->_scale;
  double y = to->y * _user->_scale;
  double x1 = 1.0/3.0 * _user->_last_x + 2.0/3.0 * cx;
  double y1 = 1.0/3.0 * _user->_last_y + 2.0/3.0 * cy;
  double x2 = 2.0/3.0 * cx             + 1.0/3.0 * x;
  double y2 = 2.0/3.0 * cy             + 1.0/3.0 * y;
    
  cairo_curve_to(_user->_context, x1, y1, x2, y2, x, y);

  _user->_last_x = x;
  _user->_last_y = y;

  return 0;
}

static int
ft2_cubic_to(FT_Vector const *control1, FT_Vector const *control2, FT_Vector const *to, void *user)
{
  // std::cout << "ft2_cubic_to: "
  //           << control1->x << ", " << control1->y << "  "
  //           << control2->x << ", " << control2->y << "  "
  //           << to->x       << ", " << to->y
  //           << std::endl;
  FT2GeomData *_user = static_cast<FT2GeomData *>(user);
  double x1 = control1->x * _user->_scale;
  double y1 = control1->y * _user->_scale;
  double x2 = control2->x * _user->_scale;
  double y2 = control2->y * _user->_scale;
  double x = to->x * _user->_scale;
  double y = to->y * _user->_scale;

  cairo_curve_to (_user->_context, x1, y1, x2, y2, x, y);

  _user->_last_x = x;
  _user->_last_y = y;

  return 0;
}

FT_Outline_Funcs ft2_outline_funcs_x = {
  ft2_move_to,
  ft2_line_to,
  ft2_conic_to,
  ft2_cubic_to,
  0, 0
};

void
get_cairo_path_for_glyph(cairo_t* context, FT_Face ft_face, unsigned int glyph_id, bool load_vertical)
{
  // Warning: measurements are in 26.6 or 16.16 format if FT_LOAD_NO_SCALE is not passes in.
  int ft_load_flags = FT_LOAD_NO_SCALE; // Implies FT_LOAD_NO_HINTING and FT_LOAD_NO_BITMAP
  if (load_vertical) {
    ft_load_flags |= FT_LOAD_VERTICAL_LAYOUT;
  }

  if (FT_Load_Glyph (ft_face, glyph_id, ft_load_flags)) {
    // Glyph not loaded!
  } else {
    // Glyph loaded!
    FT2GeomData user(context, 1.0/ft_face->units_per_EM); // Store as one unit per em.
    FT_Outline_Decompose (&ft_face->glyph->outline, &ft2_outline_funcs_x, &user);
    cairo_close_path (context);
  }
}
