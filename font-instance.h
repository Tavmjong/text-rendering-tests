
#ifndef INKSCAPE_FONT_INSTANCE_H
#define INKSCAPE_FONT_INSTANCE_H

#include <unordered_map>

#include <pangomm.h>

#include <ft2build.h>
#include FT_OUTLINE_H

namespace Cairo {
    class Path;
}

enum BASELINE {
    BASELINE_ALPHABETIC,
    BASELINE_IDEOGRAPHIC,
    BASELINE_HANGING,
    BASELINE_MATHEMATICAL,
    BASELINE_CENTRAL,
    BASELINE_MIDDLE,
    BASELINE_TEXT_BEFORE_EDGE,
    BASELINE_TEXT_AFTER_EDGE,
};
const int BASELINE_SIZE = 7;

class FontInstance {

public:
    FontInstance(Glib::RefPtr<Pango::Font> pango_font);
    virtual ~FontInstance() {};

    Glib::RefPtr<Pango::Font>        get_pango_font()    { return _pango_font; }
    FT_Face                          get_freetype_face() { return _ft_face; }
    hb_font_t*                       get_hb_font()       { return _hb_font; }
    Cairo::RefPtr<Cairo::FtFontFace> get_cairo_face()    { return _cairo_ft_face; }
 
    Pango::FontDescription get_description() { return _description; }

    // The following all are in em size units. Multiply by font size to get real size.

    // Font wide geometry
    double  get_typo_ascent()   { return _ascent; }
    double  get_typo_descent()  { return _descent; }
    double  get_max_ascent()    { return _ascent_max; }
    double  get_max_descent()   { return _descent_max; }
    double  get_x_height()      { return _xheight; }
    int     get_units_per_em()  { return _units_per_em; }
    void    get_font_metrics(double& ascent, double& descent, double& leading);
    void    get_decorations(double& underline_position,  double& underline_thickness,
                            double& strikethrough_position, double& strikethrough_thickness);
    double  get_baseline(BASELINE baseline) { return _baselines[baseline]; }

    void    get_caret_slope(double &run, double &rise) { run = _caret_run; rise = _caret_rise; }

    // Glyph related parameters
    // double  get_advance(int glyph_id, bool vertical);
    //      get_bbox(int glyph_id);

    void set_cairo_font(Cairo::RefPtr<Cairo::Context>& context, bool is_vertical, double font_size);

    Cairo::Path* get_cairo_path_for_glyph(unsigned int glyph_id);

private:

    void find_baselines();

    // The font/face in various forms
    Glib::RefPtr<Pango::Font>        _pango_font;
    FT_Face                          _ft_face = nullptr;
    hb_font_t*                       _hb_font = nullptr; // Face with set size
    hb_face_t*                       _hb_face = nullptr;
    Cairo::RefPtr<Cairo::FtFontFace> _cairo_ft_face;  // No size

    // Pango font description without size.
    Pango::FontDescription _description;
    bool _is_vertical = false;
    Pango::Gravity _gravity = Pango::GRAVITY_EAST; // Only used for vertical text.
    hb_direction_t _hb_direction = HB_DIRECTION_LTR;

    // Font wide geometry
    double _units_per_em = 1024.0; // Design units; double as we'll use it to divide.

    // In em units
    double _ascent      = 0.8;
    double _descent     = 0.2;
    double _leading     = 0.0; // a.k.a. gap, line-gap
    double _ascent_max  = 0.8; // Maximum glyph ascent in font.
    double _descent_max = 0.2; // Maximum glyph descent in font.
    double _xheight     = 0.5; // CSS defined fallback.
    double _baselines[BASELINE_SIZE];

    double _underline_position      = -0.1;
    double _underline_thickness     =  0.05;
    double _strikethrough_position  =  0.3;
    double _strikethrough_thickness =  0.05;

    double _caret_run  = 0.0;
    double _caret_rise = 1.0;
    // double _caret_offset = 0;

    // Glyph geometry (eventually 2Geom path?).
    std::map<int, Cairo::Path*> glyph_paths;
};

#endif // INKSCAPE_FONT_INSTANCE_H

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
