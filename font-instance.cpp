

#include "font-instance.h"

#include <cassert>
#include <iostream>
#include <iomanip>

#include <pango/pango-utils.h>

#include FT_TRUETYPE_TABLES_H

// #include <harfbuzz/hb-ft.h>  // TEMP TEMP TEMP ==========
#include <harfbuzz/hb-ot.h>  // TEMP TEMP TEMP =========

#include <cairo/cairo.h>
#include <cairo/cairo-ft.h>

//#define DEBUG_FONT_INSTANCE

FontInstance::FontInstance(Glib::RefPtr<Pango::Font> pango_font)
    : _pango_font(pango_font)
{
    if (!_pango_font) {
        std::cerr << "FontInstance::FontInstance: No Pango font!!!" << std::endl;
        return;
    }

    _description = _pango_font->describe(); // _with_absolute_size();
    auto gravity = _description.get_gravity();
    _is_vertical = gravity == Pango::GRAVITY_EAST || gravity == Pango::GRAVITY_WEST;

    // ****** HarfBuzz ******
    _hb_font = pango_font_get_hb_font(pango_font->gobj());
    if (!_hb_font) {
        std::cerr << "FontInstance::FontInstance: No HarfBuzz font!!!" << std::endl;
        return;
    }

    _hb_face = hb_font_get_face (_hb_font);


    // ****** FreeType ******
    hb_blob_t* hb_blob = hb_face_reference_blob (_hb_face);
    unsigned int hb_blob_length = 0;
    const char *blob_data = hb_blob_get_data (hb_blob, &hb_blob_length);

    static FT_Library ft_library = nullptr;
    if (!ft_library) {
        FT_Init_FreeType (&ft_library);
    }

    int face_index = 0;
    auto ft_error =
        FT_New_Memory_Face (ft_library, (const FT_Byte *)blob_data, hb_blob_length, face_index, &_ft_face);
    if (ft_error != 0) {
        std::cerr << "FontInstance::FontInstance: loading FT_FACE failed: " << ft_error << std::endl;
        return;
    }
    assert (_ft_face != nullptr);
    // _ft_face = pango_fc_font_lock_face(PANGO_FC_FONT(_pango_font->gobj()));  // Deprecated!

    // Load flags passed to FT_Load_Glyph
    // NO_SCALE stops Cairo::Context::set_scaled_font() from actually being scaled!
    // int ft_load_flags = FT_LOAD_NO_SCALE; // Implies FT_LOAD_NO_HINTING and FT_LOAD_NO_BITMAP
    int ft_load_flags = FT_LOAD_NO_HINTING; // Implies FT_LOAD_NO_HINTING and FT_LOAD_NO_BITMAP

    _gravity = _description.get_gravity();

    // Override direction. Not clear what NORTH/WEST should really be.
    switch (_gravity) {
        case Pango::GRAVITY_EAST:
            _hb_direction = HB_DIRECTION_TTB;
            break;
        case Pango::GRAVITY_WEST:
            _hb_direction = HB_DIRECTION_BTT;
            break;
        case Pango::GRAVITY_NORTH:
            _hb_direction = HB_DIRECTION_RTL;
            break;
        case Pango::GRAVITY_SOUTH:
        default:
            _hb_direction = HB_DIRECTION_LTR;
    }

    // Using vertical layout messes things up!
    if ((_gravity == Pango::GRAVITY_WEST || _gravity == Pango::GRAVITY_EAST) && FT_HAS_VERTICAL(_ft_face)) {
        std::cout << "  Vertical text with vertical metrics!" << std::endl;
        if (pango_version_check(1,48,4) != nullptr && pango_version_check(1,48,1) == nullptr) {
            // Pango is 1.48.1 through 1.48.4.
            std::cout << "FontInstance::FontInstance: Loading vertical metrics" << std::endl;
            ft_load_flags |= FT_LOAD_VERTICAL_LAYOUT;
        }
    }
    _cairo_ft_face = Cairo::FtFontFace::create(_ft_face, ft_load_flags);


    // *************** GEOMETRY ****************
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nFontInstance: " << _description.to_string() << std::endl;
#endif

    // ************* Scale factors *************
    // We scale everything to a unit em-size. The scale factor has been set in
    // FontFactory::get_font_instance().

    // ****** Pango ******
    double pango_scale = _description.get_size(); // Size in pt * PANGO_SCALE
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  Pango scale: " << pango_scale << std::endl;
#endif

    // ***** HarfBuzz *****
    _units_per_em = hb_face_get_upem(_hb_face);
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nHarfBuzz  OS/2 table (or 'hhea' if Use Typo flag not set in FontForge.)" << std::endl;
    std::cout << "  HB Units per em: " << _units_per_em << std::endl;

    // Point size - ptem (should use opsz instead). Can be 0 (unset).
    std::cout << "  HB ptem: " << hb_font_get_ptem (_hb_font);
#endif

    // Points per em. Can be 0 (unset).
    unsigned int hb_x_ppem = 0;
    unsigned int hb_y_ppem = 0;
    hb_font_get_ppem (_hb_font, &hb_x_ppem, &hb_y_ppem);
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  HB x_ppem: " << hb_x_ppem << " y_ppem: " << hb_y_ppem << std::endl;
#endif

    int hb_x_scale = 0;
    int hb_y_scale = 0;
    hb_font_get_scale (_hb_font, &hb_x_scale, &hb_y_scale);
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  hb_x_scale: " << hb_x_scale << " hb_y_scale: " << hb_y_scale << std::endl;
#endif
    if (hb_x_scale != hb_y_scale) {
        std::cout << "FontInstance::FontInstance: x and y hb scale are different!" << std::endl;
    }
  
    // ********** Ascent and Descent ***********

    // ****** From Pango ******
    auto font_metrics = pango_font->get_metrics();
    auto ascent  = font_metrics.get_ascent();
    auto descent = font_metrics.get_descent();
    auto height   = pango_font_metrics_get_height (font_metrics.gobj()); // font_metrics.get_height();  // Distance between baselines.

#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nPango: OS/2 table (or 'hhea' if 'Use Typo box not checked in FontForge')." << std::endl;
    // Raw:
    std::cout << "  ascent: "  << ascent  / pango_scale
              << "  descent: " << descent / pango_scale
              << "  height: "  << height  / pango_scale
              << std::endl;
    // Normalized:
    std::cout << "  ascent: "  << ascent  / (double)height
              << "  descent: " << descent / (double)height
              << "  (Normalized)"
              << std::endl;
#endif

    // ****** From HarfBuzz ******

    hb_font_extents_t hb_font_extents;
    hb_font_get_extents_for_direction (_hb_font, _hb_direction, &hb_font_extents);
    double hb_em = hb_font_extents.ascender - hb_font_extents.descender;

#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nHarfBuzz:" << std::endl;
    // Raw:
    std::cout << "  ascender: "  << hb_font_extents.ascender  / (double)hb_y_scale
              << "  descender: " << hb_font_extents.descender / (double)hb_y_scale
              << "  line gap: "  << hb_font_extents.line_gap  / (double)hb_y_scale
              << std::endl;
    // Normalized:
    std::cout << "  ascender: "  << hb_font_extents.ascender  / hb_em
              << "  descender: " << hb_font_extents.descender / hb_em
              << "  line gap: "  << hb_font_extents.line_gap  / hb_em
              << "  (Normalized)"
              << std::endl;
#endif

    // ****** From FreeType ******

    double units_per_EM = _ft_face->units_per_EM; // Scale!
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nFreeType:" << std::endl;
    std::cout << "  units_per_em: " << (int)units_per_EM << std::endl;
#endif
    if (units_per_EM == 0) {
        std::cerr << "FontInstance::FontInstance: Bitmap fonts not supported!" << std::endl;
    } else {
        // Scaleable font

        TT_OS2*  os2 = (TT_OS2*)FT_Get_Sfnt_Table (_ft_face, FT_SFNT_OS2 );       
        if (os2) {
            _ascent    = std::abs(os2->sTypoAscender)  / units_per_EM;
            _descent   = std::abs(os2->sTypoDescender) / units_per_EM;
            _xheight   = std::abs(os2->sxHeight)       / units_per_EM;
#ifdef DEBUG_FONT_INSTANCE
            std::cout << "  OS/2 Typo: "
                      << "  ascender: "  << _ascent
                      << "  descender: " << _descent
                      << "  x-height: "  << _xheight
                      << std::endl;
            std::cout << "  Default:   "
                      << "  ascender: "  << _ft_face->ascender  / units_per_EM
                      << "  descender: " << _ft_face->descender / units_per_EM
                      << "  height: "    << _ft_face->height    / units_per_EM
                      << std::endl;
#endif

            double em = _ascent + _descent;
            if (em != 1.0) {
                std::cout << "  em not equal to 1.0!!! " << em << std::endl;
            } 
            if (em > 0) {
                // Normalize (per CSS)
                _ascent   /= em;
                _descent  /= em;
                _xheight  /= em;
            }

#ifdef DEBUG_FONT_INSTANCE
            std::cout << "  OS/2 Type: "
                      << "  ascender: "  << _ascent
                      << "  descender: " << _descent
                      << "  x-height: "  << _xheight
                      << "  (Normalized)"
                      << std::endl;
#endif
        } else {
            std::cerr << "FontInstance::FontInstance: No OpenType tables!" << std::endl;
            // std::cout << "FontInstance::FontInstance: Using OpenType HHEA table!" << std::endl;
        } // TEMP TEMP BRACKETS
        { // TEMP TEMP BRACKETS
            // Fallback to 'hhea' table.
            TT_HoriHeader* hhea = (TT_HoriHeader*)FT_Get_Sfnt_Table (_ft_face, FT_SFNT_HHEA);
            if (hhea) {
#ifdef DEBUG_FONT_INSTANCE
                std::cout << "  HHEA:      "
                          << "  ascender: "     << hhea->Ascender  / units_per_EM
                          << "  descender: "    << hhea->Descender / units_per_EM
                          << "  line gap: "     << hhea->Line_Gap  / units_per_EM
                          << "  caret rise: "   << hhea->caret_Slope_Rise
                          << "  caret run: "    << hhea->caret_Slope_Run
                          << "  caret offset: " << hhea->caret_Offset     / units_per_EM
                          << std::endl;
            } else {
                std::cout << "  No HHEA table!" << std::endl;
#endif
            }

            TT_VertHeader* vhea = (TT_VertHeader*)FT_Get_Sfnt_Table (_ft_face, FT_SFNT_VHEA);
            if (vhea) {
#ifdef DEBUG_FONT_INSTANCE
                std::cout << "  VHEA:      "
                          << "  ascender: "     << vhea->Ascender  / units_per_EM
                          << "  descender: "    << vhea->Descender / units_per_EM
                          << "  line gap: "     << vhea->Line_Gap  / units_per_EM
                          << "  caret rise: "   << vhea->caret_Slope_Rise
                          << "  caret run: "    << vhea->caret_Slope_Run
                          << "  caret offset: " << vhea->caret_Offset     / units_per_EM
                          << std::endl;
            } else {
                std::cout << "  No VHEA table!" << std::endl;
#endif
            }
        }
    }

    hb_position_t position;
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  Final:    "
              << "  _ascent: "  << _ascent
              << "  _descent: " << _descent
              << "  _leading: " << _leading
              << "  _xheight: " << _xheight
              << std::endl;

    // *************** x height ****************
    std::cout << "\nx height:" << std::endl;

    // ****** From FreeType ******
    std::cout << "\nFreeType: " << _xheight << std::endl;

    // ****** From HarfBuzz ******
    std::cout << "\nHarfBuzz: ";
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_X_HEIGHT, &position)) {
        std::cout << position / (double)hb_y_scale << std::endl;
    } else {
        std::cout << "not found" << std::endl;
    }
#endif

    // Measure 'x-height' if not found above.
    if (_xheight == 0.0) {
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "  x-height not defined!" << std::endl;
#endif
    } // TEMP TEMP BRACKETS
    { // TEMP TEMP BRACKETS
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "\nHarfBuzz measured:";
#endif
        hb_codepoint_t x_glyph = 0;
        hb_font_get_nominal_glyph (_hb_font, 'x', &x_glyph);
        hb_glyph_extents_t extents;
#ifdef DEBUG_FONT_INSTANCE
        if (hb_font_get_glyph_extents (_hb_font, x_glyph, &extents)) { // 'x'
            std::cout << "  height: "    << extents.height    / (double)hb_y_scale
                      << "  y_bearing: " << extents.y_bearing / (double)hb_y_scale
                      << "  x_glyph: "   << x_glyph
                      << std::endl;
        } else {
            std::cout << "  hb: failed to get 'x' metrics!" << std::endl;
        }
#endif
    }

    FT_UInt index = FT_Get_Char_Index (_ft_face, 'x');
    FT_Load_Glyph (_ft_face, index, FT_LOAD_NO_SCALE);
    double ft_xheight = std::abs(_ft_face->glyph->metrics.height) / _units_per_em;
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nFreeType measured: ";
    std::cout << ft_xheight << std::endl;
#endif

    _xheight = ft_xheight;

    // After finding x height!
    find_baselines();


    // *********** Text Decorations ************
    _underline_position      = font_metrics.get_underline_position()      / pango_scale;
    _underline_thickness     = font_metrics.get_underline_thickness()     / pango_scale;
    _strikethrough_position  = font_metrics.get_strikethrough_position()  / pango_scale;
    _strikethrough_thickness = font_metrics.get_strikethrough_thickness() / pango_scale;

#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nText decorations:" << std::endl;
    std::cout << "\nPango:" << std::endl;
    std::cout << "  Underline position: " << _underline_position
              << "  Underline thickness: "    << _underline_thickness
              << std::endl;
    std::cout << "  Strike through position: " << _strikethrough_position
              << "  Strike through thickness: "    << _strikethrough_thickness
              << std::endl;

    std::cout << "\nHarfBuzz:" << std::endl;
    std::cout << "  Underline offset: ";
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_UNDERLINE_OFFSET, &position)) {
        std::cout << position / (double)hb_y_scale << std::endl;
    } else {
        std::cout << "not found" << std::endl;
    }

    std::cout << "  Underline size: ";
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_UNDERLINE_SIZE, &position)) {
        std::cout << position / (double)hb_y_scale << std::endl;
    } else {
        std::cout << "not found" << std::endl;
    }

    std::cout << "  Strikeout offset: ";
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_STRIKEOUT_OFFSET, &position)) {
        std::cout << position / (double)hb_y_scale << std::endl;
    } else {
        std::cout << "not found" << std::endl;
    }

    std::cout << "  Strikeout size: ";
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_STRIKEOUT_SIZE, &position)) {
        std::cout << position / (double)hb_y_scale << std::endl;
    } else {
        std::cout << "not found" << std::endl;
    }
#endif

    // **************** Cursor *****************
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nCursor (Horizontal):" << std::endl; // TODO: Handle Vertical text cursors

    std::cout << "\nHarfBuzz (see above for FreeType):" << std::endl;
    std::cout << "  Caret run: ";
#endif
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_HORIZONTAL_CARET_RUN, &position)) {
#ifdef DEBUG_FONT_INSTANCE
        std::cout << position << " ";
        std::cout << position / (double)hb_y_scale << std::endl;
#endif
        _caret_run = position;
    } else {
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "not found" << std::endl;
#endif
    }

#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  Caret rise: ";
#endif
    if (hb_ot_metrics_get_position (_hb_font, HB_OT_METRICS_TAG_HORIZONTAL_CARET_RISE, &position)) {
#ifdef DEBUG_FONT_INSTANCE
        std::cout << position << " ";
        std::cout << position / (double)hb_y_scale << std::endl;
#endif
        _caret_rise = position;
    } else {
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "not found" << std::endl;
#endif
    }

    double caret_slope = atan2(_caret_run, _caret_rise);
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "  Caret slope: " << caret_slope * 180 / M_PI << std::endl;
#endif

    // Additional HarfBuzz metrics include cap height, super/sub script size and positions.
}

void
FontInstance::get_font_metrics(double& ascent, double& descent, double& leading)
{
    ascent = _ascent;
    descent = _descent;
    leading = _leading;
}

void
FontInstance::get_decorations(double& underline_position,  double& underline_thickness,
                              double& strikethrough_position, double& strikethrough_thickness)
{
    underline_position = _underline_position;
    underline_thickness = _underline_thickness;
    strikethrough_position = _strikethrough_position;
    strikethrough_thickness = _strikethrough_thickness;
}

/*  ********** Internal Functions ********** */

/*
 * Find baselines for font.
 * FontForge: Element->Other Info->Horizontal/Vertical Baselines...
 */
void
FontInstance::find_baselines()
{
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nBaselines:" << std::endl;
#endif
    // Baselines defined relative to alphabetic.
    _baselines[ BASELINE_ALPHABETIC       ] = 0.0;            // Reference
    _baselines[ BASELINE_IDEOGRAPHIC      ] = -_descent;      // Recommendation
    _baselines[ BASELINE_HANGING          ] = 0.6;            // CSS fallback.
    _baselines[ BASELINE_MATHEMATICAL     ] = 0.8 * _xheight; // Guess
    _baselines[ BASELINE_CENTRAL          ] = 0.5 - _descent; // Definition
    _baselines[ BASELINE_MIDDLE           ] = 0.5 * _xheight; // Definition
    _baselines[ BASELINE_TEXT_BEFORE_EDGE ] = _ascent;        // Definition
    _baselines[ BASELINE_TEXT_AFTER_EDGE  ] = -_descent;      // Definition

    // Now, try to do better.
    bool found = false;
    hb_position_t coord;

    int hb_x_scale = 0;
    int hb_y_scale = 0;
    hb_font_get_scale (_hb_font, &hb_x_scale, &hb_y_scale);

    /* *** BASELINE TEST *** */
    // HarfBuzz returns true if there is a table even if missing entry.
    // This is fixed in 2.7.3 (me thinks).
    std::vector<Glib::ustring> baseline_names = {
        "Roman",
        "Hanging",
        "Ideo Face BL",
        "Ideo Face TR",
        "Ideo EM BL",
        "Ideo EM TR",
        "Math"
    };
    std::vector<hb_ot_layout_baseline_tag_t> baseline_tags = {
        HB_OT_LAYOUT_BASELINE_TAG_ROMAN			        ,
        HB_OT_LAYOUT_BASELINE_TAG_HANGING			,
        HB_OT_LAYOUT_BASELINE_TAG_IDEO_FACE_BOTTOM_OR_LEFT	,
        HB_OT_LAYOUT_BASELINE_TAG_IDEO_FACE_TOP_OR_RIGHT	,
        HB_OT_LAYOUT_BASELINE_TAG_IDEO_EMBOX_BOTTOM_OR_LEFT	,
        HB_OT_LAYOUT_BASELINE_TAG_IDEO_EMBOX_TOP_OR_RIGHT	,
        HB_OT_LAYOUT_BASELINE_TAG_MATH
    };
#ifdef DEBUG_FONT_INSTANCE
    std::cout << "Baseline test: " << _description.to_string() << std::endl;
    for (int i = 0; i < 7; i++) {
        std::cout << "Baseline test: " << baseline_names[i] << ": ";
        if (hb_ot_layout_get_baseline (_hb_font, baseline_tags[i], _hb_direction,
                                       HB_TAG_NONE, HB_TAG_NONE, &coord)) {
            std::cout << coord / (double)hb_y_scale << std::endl;
        } else {
            std::cout << "not found" << std::endl;
        }
    }
#endif

    /* ************ Alphabetic *********** */
    found = false;
    if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_ROMAN, _hb_direction,
                                   HB_TAG_NONE, HB_TAG_NONE, &coord)) {
        found = true;
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "  Alphabetic baseline: " << coord << std::endl;
#endif
        if (coord != 0) {
            std::cout << "  Alphabetic baseline not zero!" << std::endl;
        }
        _baselines[ BASELINE_ALPHABETIC ] = coord / (double)hb_y_scale;
    }

    if (!found && (_hb_direction == HB_DIRECTION_TTB || _hb_direction == HB_DIRECTION_BTT)) {
        // Try horizontal direction
        if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_ROMAN, HB_DIRECTION_LTR,
                                       HB_TAG_NONE, HB_TAG_NONE, &coord)) {
            found = true;
            _baselines[ BASELINE_ALPHABETIC ] = coord / (double)hb_y_scale;
        }
    }

    if (!found) {
        std::cout << "  Alphabetic baseline: not found" << std::endl;
    }


    /*  ********** Mathematical ********** */
    found = false;
    if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_MATH, _hb_direction,
                                   HB_TAG_NONE, HB_TAG_NONE, &coord)) {
        found = true;
        _baselines[ BASELINE_MATHEMATICAL ] = coord / (double)hb_y_scale;
    }

    if (!found && (_hb_direction == HB_DIRECTION_TTB || _hb_direction == HB_DIRECTION_BTT)) {
        // Try horizontal direction
        if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_MATH, HB_DIRECTION_LTR,
                                       HB_TAG_NONE, HB_TAG_NONE, &coord)) {
            found = true;
            _baselines[ BASELINE_MATHEMATICAL ] = coord / (double)hb_y_scale;
        }
    }

    if (!found) {
        // Try to get from glyph. Try center of minus sign.

        hb_codepoint_t glyph = 0;
        bool glyph_found = false;
        if (hb_font_get_nominal_glyph (_hb_font, 0x2212, &glyph)) {
            glyph_found = true;
        }

        if (!glyph_found) {
            // Fallback to hyphen.
            if (hb_font_get_nominal_glyph (_hb_font, '-', &glyph)) {
                glyph_found = true;
            }
        }
      
        if (glyph_found) {
            hb_glyph_extents_t extents;
            if (hb_font_get_glyph_extents (_hb_font, glyph, &extents)) {
#ifdef DEBUG_FONT_INSTANCE
                std::cout << "  Math: y_bearing: " << extents.y_bearing / (double)hb_y_scale << std::endl;
                std::cout << "  Math: height: "    << extents.height    / (double)hb_y_scale << std::endl;
#endif
                // height is negative!
                _baselines[BASELINE_MATHEMATICAL] =  (extents.y_bearing + extents.height/2.0) / (double)hb_y_scale;
            }
        }
    }

    if (!found) {
        std::cout << "  Mathematical baseline: not found" << std::endl;
    }


    /*  ************* Hanging ************ */
    found = false;
    if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_HANGING, _hb_direction,
                                   HB_TAG_NONE, HB_TAG_NONE, &coord)) {
        found = true;
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "  hanging baseline, try1: " << coord / (double)hb_y_scale << std::endl;
#endif
        _baselines[ BASELINE_HANGING ] = coord / (double)hb_y_scale;
    }

    if (!found && (_hb_direction == HB_DIRECTION_TTB || _hb_direction == HB_DIRECTION_BTT)) {
        // Try horizontal direction
        if (hb_ot_layout_get_baseline (_hb_font, HB_OT_LAYOUT_BASELINE_TAG_HANGING, HB_DIRECTION_LTR,
                                       HB_TAG_NONE, HB_TAG_NONE, &coord)) {
            found = true;
#ifdef DEBUG_FONT_INSTANCE
            std::cout << "  hanging baseline. try2: " << coord / (double)hb_y_scale << std::endl;
#endif
            _baselines[ BASELINE_HANGING ] = coord / (double)hb_y_scale;
        }
    }

    //if (!found) {
    { // TEMP TEMP TEMP
        // Try to get from glyph, assume it is at top of  Devanagari 'क' (per CSS recommendation)
        // TODO Add more scripts.
        hb_codepoint_t glyph = 0;
        // if (hb_font_get_nominal_glyph (_hb_font, 0x0915, &glyph)) {
        if (hb_font_get_nominal_glyph (_hb_font, 0x092E, &glyph)) { // TEMP TEMP
#ifdef DEBUG_FONT_INSTANCE
            std::cout << "  Measure hanging glyph: " << glyph << std::endl;
#endif
            hb_glyph_extents_t extents;
            if (hb_font_get_glyph_extents (_hb_font, glyph, &extents)) {
#ifdef DEBUG_FONT_INSTANCE
                std::cout << "  Hanging: y_bearing: " << extents.y_bearing / (double)hb_y_scale << std::endl;
                std::cout << "  Hanging: height:    " << extents.height    / (double)hb_y_scale << std::endl;
#endif
                _baselines[ BASELINE_HANGING ] = extents.y_bearing / (double)hb_y_scale;
            }
        }
    }

    if (!found) {
        std::cout << "  Hanging baseline: not found" << std::endl;
    }

#ifdef DEBUG_FONT_INSTANCE
    std::cout << "\nBaselines final:" << std::endl;
    std::cout << "  Text before edge:  " << _baselines[ BASELINE_TEXT_BEFORE_EDGE ] << std::endl;
    std::cout << "  Hanging:           " << _baselines[ BASELINE_HANGING          ] << std::endl;
    std::cout << "  Central:           " << _baselines[ BASELINE_CENTRAL          ] << std::endl;
    std::cout << "  Mathematical:      " << _baselines[ BASELINE_MATHEMATICAL     ] << std::endl;
    std::cout << "  Middle:            " << _baselines[ BASELINE_MIDDLE           ] << std::endl;
    std::cout << "  Alphabetic:        " << _baselines[ BASELINE_ALPHABETIC       ] << std::endl;
    std::cout << "  Ideographic:       " << _baselines[ BASELINE_IDEOGRAPHIC      ] << std::endl;
    std::cout << "  Text after edge:   " << _baselines[ BASELINE_TEXT_AFTER_EDGE  ] << std::endl;
#endif
}

// Set the Cairo context font in pixels. Note: does not influence if vertical glyphs are loaded!
void
FontInstance::set_cairo_font(Cairo::RefPtr<Cairo::Context>& context, bool is_vertical, double font_size)
{
    std::cout << "FontInstance::set_cairo_font: " << _description.to_string() << " " << font_size << std::endl;

    int i = 0;
    if (is_vertical) {
        switch (_gravity) {
            case PANGO_GRAVITY_EAST:
                i = 0;
                break;
            case PANGO_GRAVITY_SOUTH:
                // Sideways text
                i = 1;
                break;
            case PANGO_GRAVITY_WEST:
                i = 2;
                break;
            case PANGO_GRAVITY_NORTH:
                i = 3;
                break;
            default:
                std::cout << "UNHANDLED GRAVITY" << std::endl;
        }
    }

    Cairo::FontOptions font_options;
    font_options.set_hint_style(Cairo::HINT_STYLE_NONE);
    font_options.set_hint_metrics (Cairo::HINT_METRICS_OFF);

    auto font_description = _description.to_string();
    auto npos = font_description.find("@");
    if (npos != Glib::ustring::npos) {
        auto font_variations = font_description.substr(npos + 1); 
#ifdef DEBUG_FONT_INSTANCE
        std::cout << "  font_variations: " << font_variations << std::endl;
#endif
        cairo_font_options_set_variations(font_options.cobj(), font_variations.c_str()); // No C++ API!
    }

    auto font_matrix = Cairo::scaling_matrix (font_size, font_size);
    font_matrix.rotate(i * M_PI/2.0);
    std::cout << "font_matrix:\n"
              << "  " << font_matrix.xx
              << "  " << font_matrix.xy
              << "  " << font_matrix.x0 << "\n"
              << "  " << font_matrix.yx
              << "  " << font_matrix.yy
              << "  " << font_matrix.y0
              << std::endl;
    auto ctm         = Cairo::identity_matrix();
    auto ft_scaled_font = Cairo::FtScaledFont::create(_cairo_ft_face, font_matrix, ctm, font_options);

    context->set_scaled_font (ft_scaled_font);
}


// A hack to get glyph paths.
class FT2GeomData {
public:
    FT2GeomData(Cairo::RefPtr<Cairo::Context> context, double scale)
        : _context(context)
        , _scale(scale)
    {}

    Cairo::RefPtr<Cairo::Context> _context;
    double _scale = 1.0;
    double _last_x = 0.0;
    double _last_y = 0.0;
};

static int
ft2_move_to(FT_Vector const *to, void *user)
{
    // std::cout << "ft2_move_to: " << to->x << ", " << to->y << std::endl;
    FT2GeomData *_user = static_cast<FT2GeomData *>(user);
    double x = to->x * _user->_scale;
    double y = to->y * _user->_scale;

    _user->_context->move_to(x, y);

    _user->_last_x = x;
    _user->_last_y = y;

    return 0;
}

static int
ft2_line_to(FT_Vector const *to, void *user)
{
    // std::cout << "ft2_line_to: " << to->x << ", " << to->y << std::endl;
    FT2GeomData *_user = static_cast<FT2GeomData *>(user);
    double x = to->x * _user->_scale;
    double y = to->y * _user->_scale;

    _user->_context->line_to(x, y);

    _user->_last_x = x;
    _user->_last_y = y;

    return 0;
}

static int
ft2_conic_to(FT_Vector const *control, FT_Vector const *to, void *user)
{
    // std::cout << "ft2_conic_to: " << control->x << ", " << control->y << "  " << to->x << ", " << to->y << std::endl;
    FT2GeomData *_user = static_cast<FT2GeomData *>(user);
    double cx = control->x * _user->_scale;
    double cy = control->y * _user->_scale;
    double x = to->x * _user->_scale;
    double y = to->y * _user->_scale;
    double x1 = 1.0/3.0 * _user->_last_x + 2.0/3.0 * cx;
    double y1 = 1.0/3.0 * _user->_last_y + 2.0/3.0 * cy;
    double x2 = 2.0/3.0 * cx             + 1.0/3.0 * x;
    double y2 = 2.0/3.0 * cy             + 1.0/3.0 * y;
    
    _user->_context->curve_to(x1, y1, x2, y2, x, y);

    _user->_last_x = x;
    _user->_last_y = y;

    return 0;
}

static int
ft2_cubic_to(FT_Vector const *control1, FT_Vector const *control2, FT_Vector const *to, void *user)
{
    // std::cout << "ft2_cubic_to: "
    //           << control1->x << ", " << control1->y << "  "
    //           << control2->x << ", " << control2->y << "  "
    //           << to->x       << ", " << to->y
    //           << std::endl;
    FT2GeomData *_user = static_cast<FT2GeomData *>(user);
    double x1 = control1->x * _user->_scale;
    double y1 = control1->y * _user->_scale;
    double x2 = control2->x * _user->_scale;
    double y2 = control2->y * _user->_scale;
    double x = to->x * _user->_scale;
    double y = to->y * _user->_scale;

    _user->_context->curve_to(x1, y1, x2, y2, x, y);

    _user->_last_x = x;
    _user->_last_y = y;

    return 0;
}

FT_Outline_Funcs ft2_outline_funcs_x = {
    ft2_move_to,
    ft2_line_to,
    ft2_conic_to,
    ft2_cubic_to,
    0, 0
};

// Simple routine to dump a Cairo::Path.
void dump_cairo_path(Cairo::Path* path_in)
{
    cairo_path_t* path = path_in->cobj();

    std::cout << " Cairo path:" << std::endl;
    for (int i = 0; i < path->num_data; i += path->data[i].header.length) {
        cairo_path_data_t* data = &path->data[i];
        switch (data->header.type) {
            case CAIRO_PATH_MOVE_TO:
                std::cout << "  m " << data[1].point.x << ", " <<  data[1].point.y << std::endl;
                break;
            case CAIRO_PATH_LINE_TO:
                std::cout << "  l " << data[1].point.x << ", " <<  data[1].point.y << std::endl;
                break;
            case CAIRO_PATH_CURVE_TO:
                std::cout << "  c "
                          << data[1].point.x << ", " <<  data[1].point.y
                          << data[2].point.x << ", " <<  data[2].point.y
                          << data[3].point.x << ", " <<  data[3].point.y
                          << std::endl;
                break;
            case CAIRO_PATH_CLOSE_PATH:
                std::cout << "  z" << std::endl;
                break;
        }
    }
}

// To be replaced by Geom::Path?
Cairo::Path*
FontInstance::get_cairo_path_for_glyph(unsigned int glyph_id)
{
    // std::cout << "FontInstance::get_cairo_path_for_glyph: " << glyph_id << std::endl;

    auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 1, 1);
    auto context = Cairo::Context::create(surface);

    auto it = glyph_paths.find(glyph_id);
    if (it == glyph_paths.end()) {
        // Add path to Cairo context and save.

        // Warning: measurements are in 26.6 or 16.16 format if FT_LOAD_NO_SCALE is not passes in.
        int ft_load_flags = FT_LOAD_NO_SCALE; // Implies FT_LOAD_NO_HINTING and FT_LOAD_NO_BITMAP

        // LOADING WITH VERTICAL FLAG HAS NO EFFECT ON GLYPH PLACEMENT!
        // if (_is_vertical) {
        //     if (pango_version_check(1,48,3) == nullptr && pango_version_check(1,48,1) != nullptr) {
        //         ft_load_flags |= FT_LOAD_VERTICAL_LAYOUT;
        //     }
        // }

        if (FT_Load_Glyph (_ft_face, glyph_id, ft_load_flags)) {
            // Glyph not loaded!
        } else {
            // Glyph loaded!
            FT2GeomData user(context, 1.0/_ft_face->units_per_EM); // Store as one unit per em.
            FT_Outline_Decompose (&_ft_face->glyph->outline, &ft2_outline_funcs_x, &user);
            context->close_path();
            glyph_paths[glyph_id] = context->copy_path();
        }
    }

    // dump_cairo_path (glyph_paths[glyph_id]);

    return glyph_paths[glyph_id];
}

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
