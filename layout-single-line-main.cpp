
/*
 * A program to test comparing text rendering paths. Produces a PNG file as output.
 *
 *   Pango GlyphString to Cairo glyph.
 *   HarfBuzz hb_buffer to Cairo glyph.
 *   Pango GlyphString to FreeType Paths.
 *   HarfBuzz shaping, clusters, to cairo_show_glyph_string.
 *   Pango GlyphString to pango_cairo_show_glyph_string.
 *   Pango Layout directly.
 *
 * Can use: Pango pre-1.48.1, 1.48.1 or 1.48.2, or post 1.48.2 (custom).
 *
 * Compile:
 *   g++ -std=c++17 -g -o layout-single-line `pkg-config --cflags --libs pangocairo cairomm-1.0 pangomm-1.4 glibmm-2.4 freetype2 pangoft2 libxml++-3.0` -lm layout-single-line-main.cpp layout-single-line.cpp layout-style.cpp font-instance.cpp font-factory.cpp
 */

 #include <iostream>
#include <iomanip>

#include <pangomm.h>
#include <pangomm/init.h>
#include <pango/pangoft2.h>
#include <pango/pangocairo.h>

#include <libxml++/libxml++.h>
#include <libxml++/parsers/textreader.h>

#include "layout-single-line.h"
#include "layout-style.h"

int main (int argc, char **argv)
{
    std::cout << std::fixed << std::setprecision(2);

    int orientation = 0;
    int para_direction = 0;
    Pango::Direction base_direction = Pango::DIRECTION_LTR;
    Glib::ustring input_filename("layout-single-line.xml");
    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "mixed")      == 0) para_direction = 1, orientation = 0;
        if (strcmp(argv[i], "upright")    == 0) para_direction = 1, orientation = 1;
        if (strcmp(argv[i], "sideways")   == 0) para_direction = 1, orientation = 2;
        if (strcmp(argv[i], "horizontal") == 0) para_direction = 0;
        if (strcmp(argv[i], "vertical")   == 0) para_direction = 1;
        if (strcmp(argv[i], "ltr")        == 0) base_direction = Pango::DIRECTION_LTR;
        if (strcmp(argv[i], "rtl")        == 0) base_direction = Pango::DIRECTION_RTL;
        if (strcmp(argv[i], "LTR")        == 0) base_direction = Pango::DIRECTION_LTR;
        if (strcmp(argv[i], "RTL")        == 0) base_direction = Pango::DIRECTION_RTL;
        if (strcmp(argv[i], "RTL")        == 0) std::cout << "RTL" << std::endl;
        if (strstr(argv[i], "xml")        != 0) input_filename = argv[i];
    }

    std::locale::global(std::locale("")); // Or segfault when outputting UTF-8 via std::cout.
    Pango::init();
    std::cout << "Pango Version: " << pango_version_string() << std::endl;

    // PangoFontMap* server(pango_ft2_font_map_new());
    PangoFontMap* server(pango_cairo_font_map_new());  // One server per instance.

    std::cout << "\nParagraph direction: ";
    std::string file_tag;
    Pango::Gravity     base_gravity = Pango::GRAVITY_AUTO;
    Pango::GravityHint gravity_hint = Pango::GRAVITY_HINT_NATURAL;
    if (para_direction == 0) {
        // Horizontal
        std::cout << "Horizontal" << std::endl;
        base_gravity = Pango::GRAVITY_AUTO;
        gravity_hint =Pango::GRAVITY_HINT_NATURAL;
    } else {
        // Vertical
        if (orientation == 0) {
            std::cout << "Vertical mixed" << std::endl;
            file_tag += "_mixed";
            base_gravity = Pango::GRAVITY_EAST;
            gravity_hint = Pango::GRAVITY_HINT_NATURAL;
        }
        if (orientation == 1) {
            std::cout << "Vertical upright" << std::endl;
            file_tag += "_upright";
            base_gravity = Pango::GRAVITY_EAST;
            gravity_hint = Pango::GRAVITY_HINT_STRONG;
        }
        if (orientation == 2) {
            std::cout << "Vertical sideways" << std::endl;
            file_tag += "_sideways";
            base_gravity = Pango::GRAVITY_SOUTH;
            gravity_hint = Pango::GRAVITY_HINT_STRONG;
        }
    }

    std::cout << "Base gravity: " << base_gravity << std::endl;

    if (base_direction == Pango::DIRECTION_LTR) {
        std::cout << "Base direction: Left to right" << std::endl;
    } else {
        file_tag += "_rtl";
        std::cout << "Base direction: Right to left" << std::endl;
    }

    // Try to parse an XML string.
    //   if element is <p> or <text>, create a new line, reset style to default then parse style.
    //   if element is <span> or <tspan>, set style to default then parse style.
    // Need to add SVG positioning attributes!
    std::vector<Layout *> layouts;
    Layout* layout = nullptr;

    SPStyle style[5];
    style[0].set_direction (base_direction);
    style[0].set_base_gravity (base_gravity);
    style[0].set_gravity_hint (gravity_hint);
    int depth = 0;
    bool is_in_text = false; // Each element occurs twice, once on entry, once on exit.
    try {
        xmlpp::TextReader reader(input_filename);
        while (reader.read()) {
            Glib::ustring name = reader.get_name();
            int depth = reader.get_depth();
            std::cout << "\n  name: " << name
                      << "  depth: " << depth
                      << std::endl;;
            if (depth > 4) {
                std::cout << "Element nesting too deep!" << std::endl;
                break;
            }

            if (depth > 0) {
                style[depth] = style[depth-1];
            } else {
                style[depth] = SPStyle();
            }

            if (name == "p" || name == "text") {
                is_in_text = !is_in_text;
                std::cout << "    p: " << (is_in_text ? "Entering" : "Exiting") << std::endl;
                if (is_in_text) {
                    layout = new Layout(server);
                    layout->set_base_direction(base_direction);
                    layout->set_base_gravity(base_gravity);
                    layout->set_gravity_hint(gravity_hint);
                    layouts.push_back(layout);
                }
            } else if (name == "span" || name == "tspan") {
                std::cout << "   span" << std::endl;
            } else if (name == "#text") {
                std::cout << "   text node" << std::endl;
            } else {
                std::cerr << "    unknown element: " << name << std::endl;
            }

            if (reader.has_attributes()) {
                reader.move_to_first_attribute();
                do {
                    std::cout << "    " << reader.get_name()
                              << "  "   << reader.get_value()
                              << std::endl;
                    if (reader.get_name() == "style") {
                        style[depth].parse(reader.get_value());
                        break;
                    } else if (reader.get_name() == "y") {
                        std::cout << "NEED TO ADD SVG POSITIONING ATTRIBUTES" << std::endl;
                    } else {
                        std::cerr << "Unhandled attribute: " << reader.get_name() << std::endl;
                    }
                } while (reader.move_to_next_attribute());
                reader.move_to_element();
            } else {
                std::cout << "    NO attributes" << std::endl;
            }

            if (reader.has_value() && depth > 1) {
                std::cout << "    " << name
                          << "  value: |" << reader.get_value()
                          << "|  style: " << style[depth].write() << std::endl;
                Glib::ustring value = reader.get_value();
                layout->add_text(value, &style[depth]);
            } else {
                std::cout << "  no value";
            }
            std::cout << std::endl;
        }
    }
    catch (const std::exception& e) {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        exit(-1);
    }


    // Setup canvas.
    const int width = 4800;
    const int height = 6400;
    const int border = 10;
    auto surface = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
    auto context = Cairo::Context::create(surface);
    context->rectangle(border, border, width - 2*border, height - 2*border);
    context->set_source_rgb(0.9, 0.9, 0.9);
    context->fill();

    // Initial point.
    int x = 250;
    if (base_direction == Pango::DIRECTION_RTL) {
        x = width - x;
        std::cout << " Base direction: RTL, x: " << x << std::endl;
    }
    int y = 200;

    context->translate (x, y);

    for (auto layout : layouts) {
        // Itemize (break into PangoItem's.
        std::cout << "\nText: |" << layout->get_text() << "|\n" << std::endl;

        layout->itemize();

        std::cout << "Spans: " << std::endl;
        layout->dump_spans();

        layout->render(context);
        context->translate (0, 300);
    }

    // Create PNG.
    std::string output_filename("text");
    output_filename += file_tag;
    output_filename += "_";
    output_filename += pango_version_string();
    output_filename += ".png";
    surface->write_to_png(output_filename);

    return 0;
}

/*
  Local Variables:
  mode:c++
  c-file-style:"stroustrup"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
// vim: filetype=cpp:expandtab:shiftwidth=4:tabstop=8:softtabstop=4 :
